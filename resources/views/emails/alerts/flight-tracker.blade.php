@extends("emails.layouts.generic")

@section("header")

@php
	$flight = $data["data"];
	$tracker = $data["tracker"];
@endphp


<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/boarding-pass-email-receipt.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 34px; font-weight: bold; margin-top: 0; text-align: left;">Flight Found!</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">We found a {{ $tracker->type }} flight from {{ $tracker->origin }} to {{ $tracker->destination }} for ${{ $flight["total_amount"] }}</p>

	<table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation" style="margin-top: 30px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px; background-color: #ffffff; border-color: #e8e5ef; border-radius: 2px; border-width: 1px; box-shadow: 0 2px 0 rgba(0, 0, 150, 0.025), 2px 4px 0 rgba(0, 0, 150, 0.015); margin: 0 auto; padding: 0; width: 570px;">
       <!-- Body content -->
       <tr>
          <td class="content-cell" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position:relative; max-width: 100vw; padding: 32px;">
             <h2>Itinerary</h2>
             @include("flights.components.flight-plan",[
             	"baggages" => 0,
             	"numSlices" => sizeof($flight["slices"]),
             	"i" => 0,
             	"data" => [
             		"slices" => $flight["slices"],
             		"total_amount" => 0,
             		"iata_destination" => $tracker->destination,
             		"destination" => $tracker->destination
             	]
             ])
             <p>Note: Airlines may initiate itinerary changes leading up to departure. If you no longer need this tracker, please disable it from your <a href="https://venti.co/trips/tracker/view/{{ $tracker->trackerID }}">trips page</a></p>
             <h5>Ready to Book?</h5>
             <p style="width:100%; text-align:center;">
             	<a href="{{ $data['url'] }}" style="font-weight: bold; text-decoration: none; padding:10px 20px; margin-bottom: 20px; background:rgba(100,100,255); color:white; border-radius:10px">Search Flights</a>
             </p>
          </td>
       </tr>
    </table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection

@section("footer")
<table class="inner-body" align="center" width="333">
	
</table>
@endsection