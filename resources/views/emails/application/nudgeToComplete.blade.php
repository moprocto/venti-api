@extends("emails.layouts.generic")

@section("header")


<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/welcome.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 34px; font-weight: bold; margin-top: 0; text-align: left;">You're In!</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"></p>
<table class="inner-body" align="center" width="505">
	<tr>
		<td><p style=" font-size:20px;">Thank you for signing up for Venti: the fastest-growing social discovery platform for finding new adventures and people to explore the world with. We could not be happier to have you join us!</p></td>
	</tr>
	<tr>
		<td><p style=" font-size:20px;"><strong>We've been trying to recommend travel groups to you</strong>, but we can't because your account is incomplete. We get it; life gets busy! Here's how you can get everything squared away:</p></td>
	</tr>
	<tr>
		<td>
			<ol>
				@if($data['user']->photoUrl == "" || $data['user']->photoUrl == "/assets/img/profile.jpg" || $data['user']->photoUrl == "/assets/img/profile-2.jpg")
					<li style=" font-size: 20px;">Upload a profile picture that clearly shows you. If you had a photo before, it was removed because your face was not clearly shown or it violated our Terms of Service. We also ask that you <strong>not use minors in your profile picture,</strong> especially your children.</li>
				@endif
				@if($data['user']->customClaims['smsVerified'] == "N")
					<li style=" font-size: 20px;">Begin SMS verification by logging into your Venti account and navigating to your profile settings. You can do this <a href="https://venti.co/login" target="_blank">online</a> or through the <a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387" target="_blank">Venti iOS app</a>.</li>
				@endif
				@if($data['user']->customClaims['dob'] == 0)
					<li style=" font-size: 20px;">Add your date of birth to verify that you are at least 18 years of age.</li>
				@endif
				@if($data['user']->customClaims['bio'] == "nil" || $data['user']->customClaims['bio'] == "")
					<li style=" font-size: 20px;">Add a brief bio to your profile so others can learn a little bit more about you :)</li>
				@endif
				<li style=" font-size: 20px;">
					Make sure your interests and wishlist are up to date.
				</li>
			</ol>
		</td>
	</tr>
	<tr>
		<td><p style=" font-size:20px;">That's it! We truly appreciate having you as part of our story. We can't wait to see what exciting adventures you discover with everyone else on Venti.</p></td>
	</tr>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection

@section("footer")
<table class="inner-body" align="center" width="333">
	<tr>
		<td style="text-align:center;"><a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387"><img src="https://venti.co/assets/img/apple-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://instagram.com/venti.travel"><img src="https://venti.co/assets/img/instagram-logo.png" width="33"></a></td>
	</tr>
</table>
@endsection
