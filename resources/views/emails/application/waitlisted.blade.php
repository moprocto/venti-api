@extends("emails.layouts.generic")

@section("header")

<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/app-banner-email-waitlisted.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 18px; font-weight: bold; margin-top: 0; text-align: left;">Hello!</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"></p>
<table class="inner-body" align="center" width="505">
	<tr>
		<td>
            <p style="">We reviewed your application for our Navigator program and determined we're not a good fit for you right now. Below are the top three reasons we waitlist an applicant:
        	<ul style="">
        		<li>Little to no travel experience</li>
        		<li>No tour guide or travel planning experience</li>
        		<li>Social media or website links did not load or match our criteria</li>
        	</ul>
        	</p>
        	<p style="">
        	This is not the end of the road. We want to help you achieve your travel goals. Here are some next steps you should consider:
        	<ul style="">
        		@if($onventi)
        			<li>1. Successfully <a href="https://venti.co/groups">organize a trip</a> on Venti</li>
        			<li>3. Re-apply to become a Navigator, and we'll provide a $20 gift card for giving us another chance to win you over. <sup>* Conditions apply</sup></li>
        		@else
        			<li>1. Create a <a href="https://venti.co/register">Venti account</a></li>
        			<li>2. Successfully <a href="https://venti.co/groups">organize and facilitate a trip</a> on Venti</li>
        			<li>3. Re-apply to become a Navigator, and we'll provide a $20 gift card for giving us another chance to win you over.<br><sup>* Conditions apply</sup></li>
        		@endif
        	</ul>
            </p>
		</td>
	</tr>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection

@section("footer")
<table class="inner-body" align="center" width="333">
	<tr>
		<td style="text-align:center;"><a href="https://instagram.com/venti.travel"><img src="https://venti.co/assets/img/instagram-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.tiktok.com/@venti.travel"><img src="https://venti.co/assets/img/tiktok-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.pinterest.com/ventitravel"><img src="https://venti.co/assets/img/pinterest-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.facebook.com/venti.travel"><img src="https://venti.co/assets/img/facebook-logo.png" width="33"></a></td>
	</tr>
</table>
@endsection
