@extends("emails.layouts.generic")

@section("header")

<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/app-banner-email-schedule.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 18px; font-weight: bold; margin-top: 0; text-align: left;">Good News!</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"></p>
<table class="inner-body" align="center" width="505">
	<tr>
		<td><p style="">We've reviewed your application and would like to invite you to an interview.</p><p style="">Our first cohort of Navigators will have a huge impact on the success of our program. We conduct interviews to ensure we are a good fit for each other. Here's what you need to do:</p></td>
	</tr>
	<tr>
		<td>
			<ul style="">
				@if(!$onventi)
					<li><p>Create a <a href="https://venti.co/register">Venti account</a>. An account is required to schedule an interview and post trips on our platform.</p></li>
				@endif
				<li><p>Once logged in to your Venti account, click this link to <a href="https://venti.co/navigator/schedule">Schedule Your Interview</a>.</p></li>
				<li>
					<p>For a successful interview, please be prepared to discuss the following:</p>
					<ul>
						<li>Share more about your travel-leading experiences. Why are you interested in becoming a Navigator?</li>
						<li>Come up with three trips that you would like to start with. What themes and activities will make these trips stand out?</li>
						<li>How do you plan to promote your trips? What tools and support do you need from us to help you succeed?</li>
					</ul>
				</li>
				<li>
					<p>Please be prepared to briefly show Government-issued Photo ID during your interview. We require all Navigators to be ID verified.</p>
				</li>
			</ul>
			<p style="">Shortly after the interview, our team will provide a formal decision regarding your placement into the Navigator program.</p>
		</td>	
	</tr>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection

@section("footer")
<table class="inner-body" align="center" width="333">
	<tr>
		<td style="text-align:center;"><a href="https://instagram.com/venti.travel"><img src="https://venti.co/assets/img/instagram-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.tiktok.com/@venti.travel"><img src="https://venti.co/assets/img/tiktok-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.pinterest.com/ventitravel"><img src="https://venti.co/assets/img/pinterest-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.facebook.com/venti.travel"><img src="https://venti.co/assets/img/facebook-logo.png" width="33"></a></td>
	</tr>
</table>
@endsection
