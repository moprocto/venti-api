@extends("emails.layouts.generic")

@section("header")

<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
</a>

@endsection

@section("body")

<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 36px; font-weight: bold; margin-top: 0; text-align: center;">Hi, {{ $data["name"] }}!</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"></p>
<table class="inner-body" align="center" width="600">
	<tr>
		<td colspan="4"><p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: left;">Based on your travel preferences and interests, we think the four of you would make a great travel group. Click the "View Details" button to access this group and accept or decline our recommendation.</p></td>
	</tr>
	<tr>
		{!! $data['table'] !!}
	</tr>
	<tr>
		<td style="text-align:center;" colspan="4"><a href="https://venti.co/group/{{ $data['tripID'] }}" style="padding: 10px 20px; background:black; color:white; text-decoration:none; border-radius:20px;">VIEW DETAILS</a>
			<br><br><p style="font-size:16px;">Everyone must accept this invite to confirm the group. If the group is not confirmed within two weeks, this recommended group will be deleted. You can also access group recommendations from the <a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387" target="_blank">Venti mobile app</a> for iOS.</p></td>
	</tr>
</table>

@endsection

@section("footer")
<table class="inner-body" align="center" width="600">
	<tr>
		<td style="text-align:center;"><a href="https://instagram.com/venti.travel"><img src="https://venti.co/assets/img/instagram-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.tiktok.com/@venti.travel"><img src="https://venti.co/assets/img/tiktok-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.pinterest.com/ventitravel"><img src="https://venti.co/assets/img/pinterest-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.facebook.com/venti.travel"><img src="https://venti.co/assets/img/facebook-logo.png" width="33"></a></td>
	</tr>
</table>
@endsection
