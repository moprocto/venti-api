@extends("emails.layouts.generic")

@section("header")

<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/app-banner-email.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 18px; font-weight: bold; margin-top: 0; text-align: left;">Good News!</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"></p>
<table class="inner-body" align="center" width="505">
	<tr>
		<td>This email confirms your application to become a Venti Navigator. <br><br>Below is a summary of your responses.</td>
	</tr>
	<tr>
		<td>
            Name: <strong style="">{{ $request->name }}</strong><br>
            Email: <strong style="">{{ $request->email }}</strong><br>
            Website/Social Handle (optional): <strong style="">{{ $request->website }}</strong><br>
            Interested in becoming a Pro Navigator?: <strong style="">{{ $request->pro }}</strong><br>
            Number of Countries Traveled (Airports Don't Count 😉): <strong style="">{{ $request->countries }}</strong><br>
            Which of the following best describes you?: <strong style="">{{ $request->experience }}</strong><br>
            What country do you primarily live and work?: <strong style="">{{ $request->country }}</strong><br>
            Are you fluent in English?: <strong style="">{{ $request->fluent }}</strong><br>
            Do you prefer to stay in touch with travelers after each trip (as friends or acquintances)?: <strong style="">{{ $request->friends }}</strong><br>
            How many trips per year do you think you could guide?: <strong style="">{{ $request->numtrips }}</strong><br>
            What type of trips do you think you are best suited for?: <strong style="">{{ $request->triptype }}</strong><br>
            Submitted on:<strong style="">{{ $applied }}</strong><br>
		</td>
	</tr>
	<tr><td><br><br><p style="">Get a head start by creating a <a href="https://venti.co/register">Venti Account</a>. An account will be required to connect you with a member of our team and schedule your interview if necessary.</p></td></tr>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection

@section("footer")
<table class="inner-body" align="center" width="333">
	<tr>
		<td style="text-align:center;"><a href="https://instagram.com/venti.travel"><img src="https://venti.co/assets/img/instagram-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.tiktok.com/@venti.travel"><img src="https://venti.co/assets/img/tiktok-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.pinterest.com/ventitravel"><img src="https://venti.co/assets/img/pinterest-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.facebook.com/venti.travel"><img src="https://venti.co/assets/img/facebook-logo.png" width="33"></a></td>
	</tr>
</table>
@endsection
