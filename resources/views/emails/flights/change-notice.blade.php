@extends("emails.layouts.generic")

@section("header")

<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/boarding-pass-email-header.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 26px; font-weight: bold; margin-top: 0; text-align: left;">Airline Initiated Change</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"></p>
<table class="inner-body" align="center" width="505">
	<tr>
		<td>
            <p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{ $data["owner"] }} has informed us that they've made a change to your flight with booking reference: {{ $data["booking_reference"] }}. This might be a schedule change from the airline, or it could be a change that was requested by you or the passenger(s). You should make sure that the passenger(s) are aware of these changes.<br><br>We need to get confirmation from you that the passengers are happy with the changes. <strong>You can do so responding "accept" to this email.</strong> 
</p>
		</td>
	</tr>
	<tr>
		<td>
			<h2>Changes</h2>
			<ul style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
				@foreach($data["changes"] as $change)
					@if($change["departure_time_changed"])
						<li>{{ $change["segment_label"] }}: Departure changed from <strong>{{ $change["removed_departure_datetime"] }}</strong> to <strong>{{ $change["added_departure_datetime"] }}</strong></li>
					@endif
					@if($change["arrival_time_changed"])
						<li>{{ $change["segment_label"] }}: Arrival changed from <strong> {{ $change["removed_arrival_datetime"] }}</strong> to <strong>{{ $change["added_arrival_datetime"] }}</strong></li>
					@endif
				@endforeach
			</ul>
			<br><br>
			<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">If you have any questions, or passengers are not happy with these changes, please respond to this email to open a ticket with your support team. If we do not hear from you within 72 hours, we reserve the right to automatically accept these changes.</p>
            <br>
            
            <h4 style="text-align:center; margin:0; padding:0;"><a href="https://venti.co/home" target="_blank" style="text-decoration:none; background:black; color:white; padding:10px 20px; border-radius:10px;">Log In</a></h4>
            <br>
            <p style="color:#2e2e2e; text-align:center; width:100%;">This is not a marketing email. If you believe you are receiving this email in error, please contact support via admin@venti.co</p>
		</td>
	</tr>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection

@section("footer")
<table class="inner-body" align="center" width="333">
	
</table>
@endsection

