@extends("emails.layouts.generic")

@section("header")
<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/boarding-pass-email-receipt.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 34px; font-weight: bold; margin-top: 0; text-align: left; margin-bottom: 0; padding-bottom:0;">Order Confirmed!</h1>
<p style="padding:0; margin:0"><strong>Confirmation ID: {{ $data["booking_reference"] }}</strong></p>
<p style="padding:0; margin:0"><strong>Order Date: {{ \Carbon\Carbon::parse($data["timestamp"])->timezone($data["timezone"])->format("F d, Y")  }}</strong></p>
<br>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">You have successfully booked your stay at {{ $data["name"] }}. You can track this <a href="{{ env('APP_URL') }}/trips/{{ $data['refID'] }}" target="_blank">booking online</a> for updates and to request changes.</p>
<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><tbody>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
	<h2 class="aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif; box-sizing: border-box; font-size: 24px; color: #000; line-height: 1.2em; font-weight: 400; text-align: center; margin: 10px 0 0;" align="center"></h2>
</td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
	<td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
		<table class="invoice-items" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; margin: 0;">
			<tbody>
				<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
					<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">Base Fare:</td>
					<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">$ {{ number_format($data["base_amount"],2) }}</td>
				</tr>
				<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
					<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">Taxes:</td>
					<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">$ {{ number_format($data["tax_amount"],2) }}</td>
				</tr>
				<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
					<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">Additional Resort Fees:</td>
					<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">$ {{ number_format($data["fee_amount"],2) }}</td>
				</tr>
				<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
					<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 2px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">Amount Paid From Cash Balance:</td>
					<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 2px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">$ {{ number_format($data["paidAmount"],2) }}</td>
				</tr>
				@if($data["pointsApplied"] > 0)
					<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
						<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">Amount Paid From Points Balance</td>
						<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">{{ number_format($data["pointsApplied"],2) }}</td>
					</tr>
				@endif
				<tr class="total" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
					<td class="alignright" width="80%" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 2px; border-top-color: #333; border-top-style:solid; font-weight: 700; margin: 0; padding: 5px 0;" align="right" valign="top">Total @if($data["pointsApplied"] > 0)<br><span style="font-size:10px;">Includes Cash-Equivalent Value of Points</span>@endif</td>
					<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 2px; border-top-color: #333; border-top-style: solid; font-weight: 700; margin: 0; padding: 5px 0;" align="right" valign="top">$ {{ number_format($data["paidAmount"] + $data["pointsApplied"],2) }}</td>
				</tr>
				<tr>
					<td colspan="2"><h3 style="text-align:left;">Due at Accommodation: ${{ $data['due_at_accommodation_amount'] }}</h3><p style="text-align:left;">Mandatory fees or taxes that are due by the guest at the accommodation and typically cover items such as resort fees, wi-fi access, fitness centers, pools, energy charges or safe fees. The amount of the charge is subject to change. Depending on the accommodation, these may be payable on check in or check out. <strong>This fee cannot be collected during the booking process on Venti.</strong> A value of "N/A" means the accomodation has not shared resort fees with Venti, but we cannot guarantee it is zero. You are responsible for contacting the hotel to ensure your awareness of any additional fees associated with reserving this accommodation and what exactly is covered with resort/mandatory fees.</p></td>
				</tr>
				<tr>
					<td colspan="2">
						@if($data["accommodation"]["check_in_information"] != null)
							<h4 style="text-align:left;"><strong>Check in available after:</strong> {{ $data["accommodation"]["check_in_information"]["check_in_after_time"] }}</h4>
							<h4 style="text-align:left;"><strong>Check out before:</strong> {{ $data["accommodation"]["check_in_information"]["check_out_before_time"] }}</h4>
						@else
							<h4 style="text-align:left;">Check in/Check out information not provided by hotel at this time. Check in is typically after 3 p.m. local time and check out is before 11:30 a.m. local time. Always contact the hotel to confirm.</h4>
						@endif
					</td>
				</tr>
			</tbody>
		</table>
		@if($data["accommodation_special_requests"])
			<p style="width:100%; text-align:left">Special Request: {{ $data["accommodation_special_requests"] }}</p>
		@endif
	</td>

</tr>
</tbody>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 12px; line-height: 1.5em; margin-top: 0; text-align: left;">This email serves as your receipt. For post-booking support, please send an email to admin@venti.co, and include your confirmation ID.</p>
@endsection

@section("content")
	 <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation" style="margin-top: 30px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px; background-color: #ffffff; border-color: #e8e5ef; border-radius: 2px; border-width: 1px; box-shadow: 0 2px 0 rgba(0, 0, 150, 0.025), 2px 4px 0 rgba(0, 0, 150, 0.015); margin: 0 auto; padding: 0; width: 570px;">
       <!-- Body content -->
       <tr>
          <td class="content-cell" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position:relative; max-width: 100vw; padding: 32px;">
             <h2>Reservation Details</h2>
             	@include("hotels.components.hotel-result", ["data" => $data])
          </td>
       </tr>
    </table>
    <br>
    <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation" style="margin-top: 30px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px; background-color: #ffffff; border-color: #e8e5ef; border-radius: 2px; border-width: 1px; box-shadow: 0 2px 0 rgba(0, 0, 150, 0.025), 2px 4px 0 rgba(0, 0, 150, 0.015); margin: 0 auto; padding: 0; width: 570px;">
       <!-- Body content -->
       <tr>
          <td class="content-cell" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position:relative; max-width: 100vw; padding: 32px;">
             <h2>Conditions</h2>
             {!! $data["conditionsString"] !!}
          </td>
       </tr>
    </table>
    <br>
    <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation" style="margin-top: 30px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px; background-color: #ffffff; border-color: #e8e5ef; border-radius: 2px; border-width: 1px; box-shadow: 0 2px 0 rgba(0, 0, 150, 0.025), 2px 4px 0 rgba(0, 0, 150, 0.015); margin: 0 auto; padding: 0; width: 570px;">
       <!-- Body content -->
       <tr>
          <td class="content-cell" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position:relative; max-width: 100vw; padding: 32px;">
             	<h2>Need Travel Insurance?</h2>
             	<p style="padding:0; margin:0; color:black;">
             		We've partnered with Visitors Coverage so you can get an affordable insurance quote in about 5 minutes. Boarding Pass members can exchange 100 Points for $100 when they purchase insurance for any trip (on or off Venti) at least three times within a calendar year. Members may submit their exchange request to admin@venti.co after the completion of their trips. Simply provide your confirmation IDs <strong>{{ $data["booking_reference"] }}</strong> and your policy numbers from Visitors Coverage. <a href="https://www.visitorscoverage.com/?affid=36a20d3544aad">Get a Quote</a>
             	</p>
             	<br>
             	<a href="https://www.visitorscoverage.com/?affid=36a20d3544aad">
            		<img src="https://venti.co/assets/img/visitors-coverage-logo.png" style="width:100%; max-width:600px;">
            	</a>
          </td>
       </tr>
    </table>
@endsection



