@extends("emails.layouts.generic")

@section("header")

<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/boarding-pass-email-receipt.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 26px; font-weight: bold; margin-top: 0; text-align: left;">Congrats!</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"></p>
<table class="inner-body" align="center" width="505">
    <tr>
        <td>
            <p style="font-size: 14px;">Confirmation ID: {{ $data['confirmation'] }}</p>
            <p style="font-size: 14px;">Award Date: {{ $data['date'] }}</p>
            <p style="font-size: 16px;">You opened a Venti Mystery Box that revealed a {{ $data['tier'] }} level prize. Our team has been notified of your recent win and will be reaching out to coordinate.</p>
            @if($data['tier'] == "Bronze" || $data['tier'] == "Maincabin" || $data['tier'] == "Tailwind")
                <p style="font-size: 16px;">Can't travel right away? No worries! You have one year from today to activate this prize.</p>
            @else
                <p style="font-size: 16px;">Can't travel right away? No worries! You have two years from today to activate this prize.</p>
            @endif
            </p style="font-size: 16px;"> Review our <a href="https://venti.co/about/mysterybox">About page</a> to learn more about activations and other limitations.</p>
            <br>
            <p style="color:#2e2e2e; text-align:center; width:100%;">This is not a marketing email. If you believe you are receiving this email in error, please contact support via admin@venti.co</p>
        </td>
    </tr>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection

@section("footer")
<table class="inner-body" align="center" width="333">
    
</table>
@endsection
