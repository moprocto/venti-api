@extends("emails.layouts.generic")

@section("header")

<a href="http://localhost" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="{{ $data['imageURL'] }}" class="logo" alt="Laravel Logo" style="border-radius: 50%; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none; height: 250px; max-height: 250px; width: 250px;">
<h3>Order Confirmed</h3>
</a>

@endsection


@section("body")

<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 18px; font-weight: bold; margin-top: 0; text-align: left;">Get Ready for: {{ $data['title'] }}</h1>


<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{ $data['description'] }}</p>


<table class="inner-body" align="center" width="505">
	<tr>
		<td style=" font-weight: bold;">Purchase Date:<br><br></td>
		<td style="text-align:right;  font-size: 14px" width="200">
			<?php 
				$date = \Carbon\Carbon::createFromTimestamp($data["stampID"])->timezone('America/New_York');
				echo $date->format("M d, Y - h:i A");
		 	?> EST<br><br></td>
	</tr>
	<tr>
		<td>{{ $data['title'] }} Itinerary:</td>
		<td style="text-align:right" width="75">${{ $data['purchasePrice'] }}</td>
	</tr>
	@if($data['purchasePrice'] + $data['personalizationCost'] == $data['total'])
		<tr>
		<td>Personalization by {{ $data['author'] }}:</td>
		<td style="text-align:right" width="75">${{ $data['personalizationCost'] }}</td>
	</tr>
	@endif
	<tr>
		<td colspan="2"><br><br><hr></td>
	</tr>
	<tr style="border-top:1px solid silver;">
		<td></td>
		<td style="text-align:right" width="75">Total: ${{ $data['total'] }}</td>
	</tr>
</table>

<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection

@section("CTA")
<div style="width:250px; margin:0 auto; text-align: center;">
<a href="https://venti.co/itinerary/{{ $data['tripID'] }}/{{ $data['userID'] }}/{{ $data['stampID'] }}#Itinerary" class="button button-primary" target="_blank" rel="noopener" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; -webkit-text-size-adjust: none; border-radius: 4px; color: #fff; display: inline-block; overflow: hidden; text-decoration: none; background-color: #2d3748; border-bottom: 8px solid #2d3748; border-left: 18px solid #2d3748; border-right:18px solid #2d3748; border-top: 8px solid #2d3748;">View Itinerary</a>
</div>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 10px; line-height: 1.5em; margin-top: 50px; text-align: left;">This virtual product cannot be published outside of the venti app without permission.</p>

	<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 10px; line-height: 1.5em; margin-top: 0; text-align: left;">This purchase is final and is not elibible for a refund. Your satisfaction is our highest priority. Please contact admin@venti.co if you are unhappy with your purchase.</p>
@endsection

@section("footer")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 18px; font-weight: bold; margin-top: 0; text-align: left;">Special Note:</h1>
	<table>
		<tbody>
			<tr>
				<td class="header" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; padding: 25px 0; text-align: center;">
					<img src="{{ $data['authorImage'] }}" class="logo" alt="Laravel Logo" style="border-radius: 50%; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none; height: 100px; max-height: 100px; width: 100px;">
					<p style="text-align:left;">{{ $data['itineraryNotes'] }}</p>
					<p style="text-align:left;">Sincerely,</p>
					<p style="text-align:left;">{{ $data['author'] }}</p>
				</td>
			</tr>
		</tbody>
	</table>
@endsection