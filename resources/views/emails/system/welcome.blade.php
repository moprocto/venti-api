@extends("emails.layouts.generic")

@section("header")


<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/welcome.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 34px; font-weight: bold; margin-top: 0; text-align: left;">You're In!</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"></p>
<table class="inner-body" align="center" width="505">
	<tr>
		<td><p style=" font-size:20px;">Thank you for signing up for Venti: the fastest-growing social discovery platform for finding new adventures and people to explore the world with. We could not be happier to have you join us!</p></td>
	</tr>
	<tr>
		<td><p style=" font-size:20px;"><strong>Safety is our top priority</strong>, so we're sending this email because we need you verify you're a real person and not a robot. <p></td>
	</tr>
	<tr>
		<td>
			<ol>
				<li style=" font-size: 20px;">Verify your email instantly by clicking:<br><br><a href="{{ $data['verificationLink'] }}" target="_blank" style="font-weight: bold; text-decoration: none; padding:10px 20px; margin-bottom: 20px; background:rgba(100,100,255); color:white; border-radius:10px">THIS VERIFICATION LINK</a><br><br></li>
			</ol>
		</td>
	</tr>
	<tr>
		<td><p style=" font-size:20px;">That's it for now! We truly appreciate having you as part of our story. We can't wait to see what exciting adventures you discover with everyone else on Venti.</p></td>
	</tr>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection

@section("footer")
<table class="inner-body" align="center" width="333">
	<tr>
		<td style="text-align:center;"><a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387"><img src="https://venti.co/assets/img/apple-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://instagram.com/venti.travel"><img src="https://venti.co/assets/img/instagram-logo.png" width="33"></a></td>
	</tr>
</table>
@endsection
