<?php

Route::prefix('user')->group(function () {
	Route::get('/profile', [App\Http\Controllers\FirebaseController::class, 'getProfile']);
	Route::post('/profile', [App\Http\Controllers\FirebaseController::class, 'updateProfile']);
	Route::post('/profile/preferences', [App\Http\Controllers\FirebaseController::class, 'updatePreferences']);
	Route::post('/profile/read', [App\Http\Controllers\FirebaseController::class, 'viewUserProfile']);
	Route::post('/preferences', [App\Http\Controllers\FirebaseController::class, 'preferences']);
	Route::post('/permissions', [App\Http\Controllers\FirebaseController::class, 'permissions']);

	Route::post('/block', [App\Http\Controllers\FirebaseController::class, 'blockUser']);


	Route::prefix('visionboard')->group(function () {
		Route::post('/', [App\Http\Controllers\FirebaseController::class, 'visionboard']);

		Route::prefix('trip')->group(function () {
			Route::post('/', [App\Http\Controllers\FirebaseController::class, 'trip']);
			Route::post('/itinerary', [App\Http\Controllers\FirebaseController::class, 'getFullItinerary']);
			Route::post('/buddies', [App\Http\Controllers\FirebaseController::class, 'getBuddies']);
			Route::post('/buddies/add', [App\Http\Controllers\FirebaseController::class, 'addBuddy']);
			Route::post('/buddies/remove', [App\Http\Controllers\FirebaseController::class, 'removeBuddy']);
			Route::post('/archive', [App\Http\Controllers\FirebaseController::class, 'archiveTrip']);
			Route::post('/delete', [App\Http\Controllers\FirebaseController::class, 'deleteTrip']);
			Route::post('/budget', [App\Http\Controllers\FirebaseController::class, 'tripBudget']);
		});
	});

	Route::prefix('ideas')->group(function () {
		Route::post('/', [App\Http\Controllers\FirebaseController::class, 'userIdeas']);
	});

	Route::prefix('settings')->group(function () {
		Route::post('/', [App\Http\Controllers\FirebaseController::class, 'getSettings']);
		Route::post('/update', [App\Http\Controllers\FirebaseController::class, 'updateSettings']);
		Route::post('/saveFCMToken', [App\Http\Controllers\FirebaseController::class, 'saveFCMToken']);
		Route::post('/saveTrackingPermission', [App\Http\Controllers\FirebaseController::class, 'saveTrackingPermission']);
		Route::post('/sms', [App\Http\Controllers\FirebaseController::class, 'saveSMS']);
	});


	
});

Route::prefix('users')->group(function () {
	Route::post('/', [App\Http\Controllers\FirebaseController::class, 'users']);
	Route::post('/curate/Everyone', [App\Http\Controllers\FirebaseController::class, 'users']);
	Route::post('/curate/Matches', [App\Http\Controllers\FirebaseController::class, 'curateMatches']);
	Route::post('/curate/{category}', [App\Http\Controllers\FirebaseController::class, 'curateUsers']);
});

Route::prefix('invites')->group(function () {
	Route::post('/', [App\Http\Controllers\FirebaseController::class, 'invites']);
	Route::post('/groups', [App\Http\Controllers\FirebaseController::class, 'getUserGroups']);
	Route::post('/respond', [App\Http\Controllers\FirebaseController::class, 'groupInviteReply']);
});