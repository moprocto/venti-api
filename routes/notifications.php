<?php

Route::prefix('notifications')->group(function () {
	Route::post('/send', [App\Http\Controllers\NotificationsController::class, 'send'])->name('notifications-send');
});