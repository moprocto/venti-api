<?php

Route::prefix('home')->group(function () {
	Route::post('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
	
	Route::get('/change/{subscription}', [App\Http\Controllers\BoardingPassController::class, 'changeSubscription'])->name('change-subscription');

	Route::prefix('transactions')->group(function () {
		Route::post('/', [App\Http\Controllers\HomeController::class, 'transactionsTable'])->name('transactions-table');
		Route::post('/detail', [App\Http\Controllers\HomeController::class, 'transaction'])->name('transactions-detail');
		Route::post('/cancel', [App\Http\Controllers\HomeController::class, 'cancelConfirm'])->name('transactions-cancel');
		Route::post('/snapshots', [App\Http\Controllers\HomeController::class, 'snapshotsTable'])->name('snapshots-table');
		Route::post('/purchase/points', [App\Http\Controllers\BoardingPassController::class, 'purchasePoints'])->name('purchase-points');

		Route::prefix('statements')->group(function () {
			Route::post('/', [App\Http\Controllers\HomeController::class, 'statements'])->name('statements');
		});
	});

	Route::prefix('sources')->group(function () {
		Route::post('/', [App\Http\Controllers\HomeController::class, 'sources']);
		Route::prefix('detail')->group(function () {
			Route::post('/bank', [App\Http\Controllers\HomeController::class, 'sourceDetail']);
			Route::post('/card', [App\Http\Controllers\HomeController::class, 'cardDetail']);
		});
	});
});