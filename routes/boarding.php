<?php
Route::prefix('boardingpass')->group(function () {

    Route::prefix('customer')->group(function () {

        Route::prefix('status')->group(function () {
            Route::post('/', [App\Http\Controllers\BoardingPassController::class, 'checkCustomerStatus'])->name('boarding-status-customer');
            Route::post('/marketing', [App\Http\Controllers\BoardingPassController::class, 'getMarketingStatus'])->name('boarding-status-customer-marketing');
            Route::post('/marketing/tags', [App\Http\Controllers\BoardingPassController::class, 'updateMarketingStatus'])->name('boarding-status-customer-marketing-update');

            // push notifications

            Route::prefix('push-notifications')->group(function () {
                Route::post('/', [App\Http\Controllers\NotificationsController::class, 'setPushNotifications']);
                Route::post('/read', [App\Http\Controllers\NotificationsController::class, 'readPushNotification']);
                Route::post('/delete', [App\Http\Controllers\NotificationsController::class, 'deletePushNotification']);
            });

        });

        Route::prefix('balances')->group(function () {
            Route::post('/points', [App\Http\Controllers\BoardingPassController::class, 'pointsBalance']);
        });

        Route::prefix('fundingSource')->group(function () {
            Route::post('/', [App\Http\Controllers\BoardingPassController::class, 'createFundingSource'])->name('boarding-create-funding-source');

            Route::post('/remove', [App\Http\Controllers\BoardingPassController::class, 'removeFundingSource'])->name('boarding-remove-funding-source');

            Route::prefix('bank')->group(function () {
                Route::post('/detail', [App\Http\Controllers\BoardingPassController::class, 'bankDetails'])->name('bank-details');
                Route::post('/verify', [App\Http\Controllers\BoardingPassController::class, 'verifyBank'])->name('boarding-verify-bank');
                Route::post('/deposit', [App\Http\Controllers\BoardingPassController::class, 'depositToBoardingPass'])->name('deposit-to-boarding-pass');
                Route::post('/withdraw', [App\Http\Controllers\BoardingPassController::class, 'withdrawToBank'])->name('withdraw-to-bank');
            });

            Route::prefix('card')->group(function () {
                Route::post('/list', [App\Http\Controllers\BoardingPassController::class, 'getCards'])->name('boarding-get-cards');
                Route::post('/read', [App\Http\Controllers\BoardingPassController::class, 'readCard'])->name('boarding-get-cards');
                Route::post('/save', [App\Http\Controllers\BoardingPassController::class, 'saveCard'])->name('boarding-save-card');
            });
        });

        Route::post('/purchase/points', [App\Http\Controllers\BoardingPassController::class, 'purchasePoints'])->name('purchase-points');
        
        Route::prefix('transactions')->group(function () {
            Route::prefix('deposit')->group(function () {
                Route::post('/funds', [App\Http\Controllers\BoardingPassController::class, 'depositToBoardingPass'])->name('deposit-to-boarding-pass');
                Route::post('/points', [App\Http\Controllers\BoardingPassController::class, 'depositPoints'])->name('deposit-points');
            });
            Route::prefix('withdraw')->group(function () {
                Route::post('/funds', [App\Http\Controllers\BoardingPassController::class, 'withdrawToBank'])->name('withdraw-to-bank');
                Route::post('/points', [App\Http\Controllers\BoardingPassController::class, 'withdrawPoints'])->name('withdraw-points');
            });


            Route::prefix('mysterybox')->group(function () {
                Route::post('/', [App\Http\Controllers\BoardingPassController::class, 'mysterybox'])->name('mysterybox');
                Route::post('/opens', [App\Http\Controllers\BoardingPassController::class, 'opens'])->name('mysterybox');
            });

            
            Route::post('/superpowers', [App\Http\Controllers\BoardingPassController::class, 'superpowers'])->name('superpower');
            Route::post('/superpowers/activate', [App\Http\Controllers\BoardingPassController::class, 'superpowerActivate'])->name('superpower-activate');
        });
    });

    Route::get('/share', [App\Http\Controllers\BoardingPassController::class, 'share'])->name('boarding-share');
    Route::post('/share/points', [App\Http\Controllers\BoardingPassController::class, 'sharePoints'])->name('share-points');
    Route::get('/track/{code}', [App\Http\Controllers\BoardingPassController::class, 'track'])->name('boarding-track');

});

Route::get('/auth/mfa', [App\Http\Controllers\BoardingPassController::class, 'authMFA'])->name('auth-mfa');