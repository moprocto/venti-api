<?php
	Route::prefix('pro')->group(function () {
		Route::post('/account', [App\Http\Controllers\ProController::class, 'getBank'])->name('get-plaid-bank');
		Route::post('/account/remove', [App\Http\Controllers\ProController::class, 'deleteBank'])->name('get-plaid-bank');
		Route::post('/subaccount/remove', [App\Http\Controllers\ProController::class, 'deleteSubaccount'])->name('get-plaid-bank');
		Route::post('/list', [App\Http\Controllers\ProController::class, 'getBanks'])->name('list-plaid-banks');
		Route::post('/list/subaccounts', [App\Http\Controllers\ProController::class, 'getSubaccounts'])->name('list-plaid-subaccounts');

		Route::prefix('bank')->group(function () {
			Route::post('/transactions', [App\Http\Controllers\ProController::class, 'getBankTransactions'])->name('transactions-plaid-bank');
			Route::prefix('subaccounts')->group(function () {
				Route::post('/', [App\Http\Controllers\ProController::class, 'getBankSubaccounts'])->name('list-plaid-subaccounts');
				Route::post('/balance', [App\Http\Controllers\ProController::class, 'getBankSubaccountBalance'])->name('balance-plaid-subaccounts');
			});
		});
	});
