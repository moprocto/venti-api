<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Kreait\Firebase\Exception\FirebaseException;
use Symfony\Component\HttpFoundation\Response;
use Validator;

use App\Models\Token as Token;

class FirebaseAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  Symfony\Component\HttpFoundation\Response $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // Flatten the headers
        $flattenedHeaders = [];
        foreach ($request->headers->all() as $key => $value) {
            $flattenedHeaders[$key] = $value[0]; // Take the first value of each header
        }

        $validator = Validator::make($flattenedHeaders, [
            'uid' => 'required|string',
            'x-api-key' => 'required|string|min:42',
        ]);

        list($uid, $apiKey) = [$request->headers->get('uid'), $request->headers->get('x-api-key')];

        // passed header key request

        if ($validator->fails()) {
            // not all required fields presented
            return new Response('Error: Invalid Headers', 400);
        }

        $firestore = app('firebase.firestore');
        $auth = app('firebase.auth');

        try{
            $user = $auth->getUser($uid);
            if($user->disabled){
                return new Response('Error: Unauthorized User', 401);
            }

            $customClaims = $user->customClaims;
            $token = new Token($customClaims["api_token"] ?? null, $customClaims["api_token_expiry"] ?? null);
            $token->createIfNew($auth, $user->uid, $customClaims);

            if($token->isExpired() || $token->token != $apiKey){
                //return new Response('Error: Invalid API Token', 402);
            }

            return $next($request);
        }
        catch(FirebaseException $e){
            return new Response('Error: Unauthorized User', 401);
        }
    }
}