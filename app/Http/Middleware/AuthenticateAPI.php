<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Token as Token;
use Carbon\Carbon as Carbon;

class AuthenticateAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiKey = $request->headers->get('x-api-key');

        if($apiKey == null){
            return new Response('Error: Missing Authentication', 500);
        }

        // check for validity of token



        return $next($request);
    }
}
