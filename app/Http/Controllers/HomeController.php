<?php

namespace App\Http\Controllers;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Google\Cloud\Firestore\FirestoreClient;
use Kreait\Firebase\Exception\FirebaseException;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Http;

use App\Models\Transaction as Transaction;
use App\Models\User as User;

use Illuminate\Http\Request;
use Redirect;
use Dwolla;
use Session;
use Validator;
use Mail;

use \Stripe\Stripe as Stripe;
use \Stripe\StripeClient as StripeClient;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('firebase.auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request){

        // check if maintenance mode is engaged.

        /* response codes

            405: application offline for maintenance
            406: email verification required
            407: sms verification required
            408: application not paid
    

        */


        if(env('APP_DOWN') == "true"){
            return json_encode(405); // down for maintenance
        }

        // we need to check to see if they have any onboarding steps

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $request->header('uid');


        try{
            $user = $auth->getUser($user);

            if(!$user->emailVerified){
                return json_encode(406);
            }

            $customClaims = $user->customClaims ?? false;

            if(!$customClaims["smsVerified"]){
                return json_encode(407);
            } else{

                // check if the current session has 2FA'd

            }

            // the user has completed all the basic steps to create a Venti Account
        }
        catch(FirebaseException $e){
            // error page for when we cannot find the account

        }

        // get customer data



        $dwolla = new Dwolla;

        $customer = $dwolla->getCustomer($user, $auth, $db);

        $fundingSources = [];
        $fundingSourcesCount = 0;

        $transactable = $wallet = $secret = $appPaid = false;

        $subscription = $user->customClaims["subscription"] ?? "buddy";



        if($customer){
            $fundingSources = $dwolla->fundingSources($customer["customerID"]);

            $appPaid = $user->customClaims["appPaid"] ?? false;

            if($fundingSources){
                foreach($fundingSources as $fundingSource){
                    if(!$fundingSource->removed 
                        && isset($fundingSource->bankAccountType)
                        && $fundingSource->status == "verified"
                    ){
                        // this user can now make deposits and withdrawals using their bank account

                        $transactable = true;
                        $fundingSourcesCount++;
                    }
                    if($fundingSource->type == "balance"){
                        $wallet = explode("funding-sources/",$fundingSource->_links->self->href)[1];
                    }
                    if(!$fundingSource->removed 
                        && isset($fundingSource->bankAccountType)
                        && $fundingSource->status == "unverified"
                    ){
                        $fundingSourcesCount++;
                    }
                }
                // account balances
            }
        }

        else{
            // we need to see if the end user has already paid their application fee

            

            $appPaid = $user->customClaims["appPaid"] ?? false;

            if(!$appPaid){
                
                return json_encode(408);
                
            }
        }

        // see if warp drive is available

        $firebaseUser = new User($user->uid);
        $transactions = $firebaseUser->transactions();
        $canPointsPurchasePoints = $firebaseUser->canPointsPurchasePoints($user->uid, $db);


        // we are pulling the subscription value from the customClaims record in firebase
        // for those that signed up before Jan 1, they will not have a subscription attribute
        // by default, we will treat them as interested in buddy pass

        // get age of the account

        $createdAt = $user->metadata->createdAt->format("Y-m-d");

        $creationDate = new Carbon($createdAt);

        $accountAge = $creationDate->diffInDays(Carbon::now());

        // balance details

        $walletDetails = $dwolla->getFundingSourceBalance($wallet);

        $wallet = $transactions = [];

        if(!$walletDetails){
            return json_encode(408);
        }

        $balance = (float) $walletDetails->balance->value;
        $wallet["last_updated"] = \Carbon\Carbon::parse($walletDetails->last_updated)->format("m-d-Y H:i:s");
    
        $points = $firebaseUser->getPoints();

        $spendingPower = $balance + $points;

        $entries = getSweepstakeEntries($subscription, $balance, $points);
        $ranking = $user->customClaims["leaderboard"] ?? "X";

        $data = [
            "image" => "https://venti.co/assets/img/boarding-pass-social.jpg",
            "url" => "https://venti.co/boardingpass",
            "user" => $user,
            "customer" => $customer,
            "fundingSources" => $fundingSources,
            "transactable" => $transactable,
            "transactions" => $transactions,
            "wallet" => $wallet,
            "balance" => "$" . number_format($balance,2),
            "points" => number_format($points,2),
            "power" => "$" . number_format($spendingPower,2),
            "canPointsPurchasePoints" => $canPointsPurchasePoints,
            "subscription" => $subscription,
            "upgrade" => "false",
            "accountAge" => $accountAge,
            "entries" => number_format($entries,0),
            "fundingSourcesCount" => $fundingSourcesCount,
            "eligible" => $customer["status"],
            "ranking" => $ranking
        ];

        return json_encode($data);
    }

    public function sources(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $user = $auth->getUser($request->header('uid'));

        $dwolla = new Dwolla;

        $customer = $dwolla->getCustomer($user, $auth, $db);

        $fundingSourcesList = [];
        $fundingSourcesList["banks"] = [];
        $fundingSourcesList["cards"] = [];
        $fundingSourcesCount = 0;

        $transactable = $wallet = $secret = $appPaid = false;

        $subscription = $user->customClaims["subscription"] ?? "buddy";

        if($customer){
            $fundingSources = $dwolla->fundingSources($customer["customerID"]);

            if($fundingSources){
                foreach($fundingSources as $fundingSource){
                    if(isset($fundingSource->bankAccountType) && $fundingSource->name != "Balance"){
                        if(!$fundingSource->removed 
                            && isset($fundingSource->bankAccountType)
                            && $fundingSource->status == "verified"
                        ){
                            array_push($fundingSourcesList["banks"], $fundingSource);
                            // this user can now make deposits and withdrawals using their bank account
                        }
                        if(!$fundingSource->removed 
                            && isset($fundingSource->bankAccountType)
                            && $fundingSource->status == "unverified"
                        ){
                            array_push($fundingSourcesList["banks"], $fundingSource);
                        }
                    }
                }
                // account balances
            }
        }

        $cards = \Stripe::listCards($db, $user->uid);

        foreach($cards as $card){
            array_push($fundingSourcesList["cards"], $card);
        }

        return json_encode($fundingSourcesList);
    }

    public function sourceDetail(Request $request){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $source = $db->collection("Sources")->document($request->id)->snapshot()->data();
        $source["status"] = $source["status"] ?? "Unverified";

        $dwolla = new Dwolla;
        $source = $dwolla->getFundingSource($source["source"]);


        return json_encode($source);
    }

    public function cardDetail(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $card = $db->collection("Cards")->document($request->id)->snapshot()->data();

        $card["cardNumber"] = "⬤⬤⬤⬤ - ⬤⬤⬤⬤ - ⬤⬤⬤⬤ - " . $card["cardLast4"];
        
        return json_encode($card);
    }
    

    public function transaction(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $id = $request->get('id');

        $user = $auth->getUser($request->header('uid'));

        // check to see if the logged in user matches the user id
        // or if the user is an admin

        $transaction = new Transaction;
        $transaction = $transaction->get($id, $user);
        
        // I need a separate firebase instance for sandboxing

        if($transaction == null){
            return json_encode(500);
        }

        $data = [ 
            "id" => $id,
            "note" => $transaction["note"],
            "category" => $transaction["category"],
            "from" => $transaction["from"],
            "to" => $transaction["to"],
            "fee" => $transaction["fee"],
            "total" => $transaction["total"],
            "timestamp" => $transaction["strtotime"],
            "date" => $transaction["timestamp"],
            "status" => $transaction["status"],
            "cancelLink" => $transaction["transactionCancelLink"] ?? false
        ];

        return json_encode($data);
    }

    public function transactionsTable(Request $request){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->header('uid'));

        // check to see if the logged in user matches the user id
        // or if the user is an admin

        if(!isAdmin($user)){
            if($user->uid != $request->headers->get('uid')){
                return json_encode(500);
            }
        }



        $transactionDocs = $db->collection("Transactions");
        $transactionDocs = $transactionDocs
        ->where("uid","=",$request->headers->get('uid'))
        ->documents();
        // I need a separate firebase instance for sandboxing

        $transactions = [];

        foreach($transactionDocs as $transactionDoc){
            $data = $transactionDoc->data();

            $timestamp = \Carbon\Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->format("M. d, Y");
            $ampm = \Carbon\Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->format("h:i A");
            $strtotime = \Carbon\Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->timestamp;
            $category = "Transfer";

            $type = "";
            $negative = false;

            if($data["type"] == "points-deposit" || $data["type"] == "points-withdraw"){
                $category = "Points";
            }

            if($data["type"] == "withdraw" || $data["type"] == "points-withdraw"){
                $negative = true;
            }

            $total = number_format($data["total"],2);

            if($category != "Points"){
                $type = "<br> Type: ACH " . ucfirst($data["type"]);
                $total = "$" . $total;
            }

            if($negative){
                // we show a negative sign for withdraw transactions
                $total = "-$total";
            }

            $status = ucfirst($data["status"]);

            $transactionID = explode("transfers/",$data['transactionUrl'])[1] ?? "";

            $btnClass = "";

            if($status == "Pending"){
                if(!str_contains($data['note'], "Fee")){
                    $btnClass = "text-warning";
                    
                }else{
                    $btnClass = "text-success";
                    $status = 'Complete';
                }
                
            }
            if($status == "Complete" || $status == "Canceled"){
                $btnClass = "text-success";
            }

            array_push($transactions, [
                "id" => $data["transactionID"],
                "description" => $data["note"] . '<br>ID: ' . $data["transactionID"],
                "category" => $category,
                "from" => $data["from"],
                "to" => $data["to"],
                "total" => $total,
                "timestamp" => $strtotime,
                "date" => $timestamp,
                "status" => $status,
            ]);
        }

        // sort transactions

        usort($transactions, function($a, $b) {
            return $b['timestamp'] <=> $a['timestamp'];
        });

        return json_encode($transactions);
    }

    public function cancelConfirm(Request $request){
        $id = $request->get('id');
        $dwolla = new Dwolla;
        $cancel = $dwolla->cancelTransaction($id);

        if($cancel){
            return json_encode(200);
        }

        return json_encode(500);
    }

    public function statements(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        
        // check to see if the logged in user matches the user id
        // or if the user is an admin

        $user = $auth->getUser($request->header('uid'));

        $transactionDocs = $db->collection("BalanceSnapshots");
        $transactionDocs = $transactionDocs
        ->where("uid","=",$request->user)
        ->where("reconciled","=",true)
        ->where("statement","=",true)
        ->documents();
        // I need a separate firebase instance for sandboxing

        $statements = [];

        foreach($transactionDocs as $transactionDoc){
            $data = $transactionDoc->data();

            $timestamp = \Carbon\Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->format("M. d, Y");

            $ampm = \Carbon\Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->format("h:i A");

            $strtotime = \Carbon\Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->timestamp;

            array_push($statements, [
                "id" => $data["snapshotID"],
                "month" => $data["month"],
                "year" => $data["year"],
                "balance" => $data["balance"],
                "points" => $data["points"] ?? 0,
                "strtotime" => $strtotime,
                "timestamp" => $timestamp
            ]);
        }

        return json_encode($statements);
    }



    public function viewStatement(Request $request){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');


        $transactionDocs = $db->collection("BalanceSnapshots");
        $transactionDocs = $transactionDocs
        ->where("uid","=",$user->uid)
        ->where("snapshotID","=",$id)
        ->documents();

        $data = "";

        foreach($transactionDocs as $transactionDoc){
            $data = $transactionDoc->data();
        }

        $userTimezone = $user->customClaims["timezone"] ?? "America/New_York";

        $timestamp = \Carbon\Carbon::parse($data["timestamp"])
        ->timezone($userTimezone)->format("M. d, Y");

        $ampm = \Carbon\Carbon::parse($data["timestamp"])->timezone($userTimezone)->format("h:i A");

        $strtotime = \Carbon\Carbon::parse($data["timestamp"])->timezone($userTimezone)->timestamp;

        $year = \Carbon\Carbon::parse($data["timestamp"])->timezone($userTimezone)->format("Y");
        
        $month = \Carbon\Carbon::parse($data["timestamp"])->timezone($userTimezone)->format("m");

        // get all transaction within the statement period

        //dd([$timestamp, $strtotime, $ampm, $year, $month]);

        $lastMonth = \Carbon\Carbon::parse($data["timestamp"])->timezone($userTimezone)->subMonth()->timestamp;

        $transactions = $db->collection("Transactions");
        $transactions = $transactions
        ->where("uid","=", $user->uid)
        ->orderBy("timestamp", "DESC")
        ->documents();
        //

        $statementTransactions = [];

        $transactionValue = $pointsValue = 0;

        foreach($transactions as $transaction){
            $transaction = $transaction->data();

            if($transaction["timestamp"] >= $lastMonth && $transaction["timestamp"] <= $strtotime){
                array_push($statementTransactions, $transaction);

                $negative = false;

                if($transaction["type"] == "deposit" || $transaction["type"] == "withdraw"){
                    $amount = $transaction["amount"];

                    if($transaction["type"] == "withdraw"){
                        $amount *= -1;
                    }

                    $transactionValue += $amount;
                }

                if($transaction["type"] == "points-deposit" || $transaction["type"] == "points-withdraw"){

                    $amount = $transaction["amount"];

                    if($transaction["type"] == "points-withdraw"){
                        $amount *= -1;
                    }

                    $pointsValue += $amount;
                }
            }
        }

        usort($statementTransactions, function($a, $b) {
            return $a['timestamp'] <=> $b['timestamp'];
        });

        // get a sum of the deposits/withdraws



        $data = [
            "id" => $month,
            "data" => $data,
            "transactions" => $statementTransactions,
            "transactionValue" => number_format($transactionValue,2),
            "pointsValue" => number_format($pointsValue,3),
            "timezone" => $userTimezone,
            "month" => $data["month"],
            "year" => $data["year"]
        ];

        return view("boardingpass.panels.transactions.statement", $data);
    }

}
