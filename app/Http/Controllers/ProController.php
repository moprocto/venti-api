<?php

namespace App\Http\Controllers;

use Carbon\Carbon as Carbon;
use Illuminate\Http\Request;
use Plaid;
use DateTime;

class ProController extends Controller
{
    public function __construct(Plaid $plaidService)
    {
        $this->plaidService = $plaidService;
    }

    public function getBanks(Request $request){
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $banks = [];

        $bankDocs = $db->collection("Plaids")
            ->where("uid", "=", $user->uid)
            ->documents();

        foreach($bankDocs as $bank){
            $bank = $bank->data();
            // get subaccounts
            $subaccountDocs = $db->collection("PlaidSubaccounts")
                ->where("access_token", "=", $bank["access_token"])
                ->documents();
            $bank["accounts"] = [];

            foreach($subaccountDocs as $subaccount){
                $subaccount = $subaccount->data();
                array_push($bank["accounts"], $subaccount);
            }

            array_push($banks, $bank);
        }

        return json_encode($banks);
    }

    public function getBank(Request $request){
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $bankaccount = [];

        $bankDocs = $db->collection("Plaids")
            ->where("vid", "=", $request->vid)
            ->documents();

        foreach($bankDocs as $bank){
            $bank = $bank->data();
            // get subaccounts
            $subaccountDocs = $db->collection("PlaidSubaccounts")
                ->where("access_token", "=", $bank["access_token"])
                ->documents();
            $bank["accounts"] = [];

            foreach($subaccountDocs as $subaccount){
                $subaccount = $subaccount->data();
                array_push($bank["accounts"], $subaccount);
            }

            $bankaccount = $bank;
        }

        return json_encode($bankaccount);
    }

    public function getSubaccounts(Request $request){
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $subaccounts = [];

        $bankDocs = $db->collection("PlaidSubaccounts")
            ->where("access_token", "=", $request->access_token)
            ->documents();

        foreach($bankDocs as $bank){
            $account = $bank->data();
            // get subaccounts
            array_push($subaccounts, $account);
        }

        return json_encode($subaccounts);
    }

    public function getBankSubaccounts(Request $request){
        $access_token = $request->access_token;
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $subaccounts = $db->collection("PlaidSubaccounts")
            ->where("access_token", "=", $request->access_token)
            ->documents();

        $accounts = [];
        $startDate = new DateTime(Carbon::now()->subDays(30)->format('Y-m-d')); // Transactions from the past 30 days
        $endDate = new DateTime(Carbon::now()->format('Y-m-d'));

        foreach($subaccounts as $subaccount){
            $subaccount = $subaccount->data();
            $transactions = $db->collection("PlaidTransactions")->where("account_id","=", $subaccount["account_id"])->documents();
            $subaccount["transactions"] = [];
            foreach($transactions as $transaction){
                $transaction = $transaction->data();
                array_push($subaccount["transactions"],$transaction);
            }
            $subaccount["balance"] = $subaccount["balances"]->available ?? 0;
            $subaccount["balanceCurrent"] = $subaccount["balances"]->current ?? 0;

            array_push($accounts, (array) $subaccount);
        }

        return json_encode($accounts);

        /*

            $plaidAccounts = $this->plaidService->getTransactions($bank["access_token"], $startDate, $endDate, null);
            $accounts = [];
            $accountList = $plaidAccounts->accounts ?? [];
            $transactionsList =  $plaidAccounts->transactions ?? [];

            foreach($accountList as $account){

                $transactions = [];

                foreach($transactionsList as $transaction){
                    if($account->account_id == $transaction->account_id){
                        array_push($transactions, $transaction);
                    }
                }

                // update the account balances each time this page loads

                $accountData = $this->findAccountById($bank["accounts"], $account->account_id);

                $account = (array) $account;

                array_push($accounts, [
                    "account_id" => $account["account_id"],
                    "balance" => $account["balances"]->available ?? 0,
                    "balanceCurrent" => $account["balances"]->current ?? 0,
                    "name" => $account["name"],
                    "official_name" => $account["official_name"],
                    "type" => $account["type"],
                    "subtype" => $account["subtype"],
                    "transactions" => $transactions,
                    "mask" => $account["mask"],
                    "boardingpass" => $accountData["boardingpass"] ?? false
                ]);
            }

        */
    }

    public function deleteSubaccount(Request $request){
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $subaccounts = [];

        $bankDocs = $db->collection("PlaidSubaccounts")
            ->where("account_id", "=", $request->vid)
            ->documents();

        foreach($bankDocs as $bank){
            $account = $bank->data();
            // get subaccounts
            if($account["uid"] == $user->uid){
                $db->collection("PlaidSubaccounts")->document($account["vid"])->delete();
                $remove = $this->plaidService->removeItem($account["item_id"]);
            }
        }

        return json_encode(200);
    }

    public function deleteBank(Request $request){
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $bankDoc = $db->collection("Plaids")->document($request->vid);

        $data = $bankDoc->snapshot()->data();

        if($data["uid"] != $user->uid){
            return json_encode(500);
        }

        $db->collection("Plaids")->document($request->vid)->delete();

        $subaccounts = $db->collection("PlaidSubaccounts")
            ->where("access_token", "=", $data["access_token"])
            ->documents();

        foreach($subaccounts as $subaccount){
            $subaccount = $subaccount->data();
            $db->collection("PlaidSubaccounts")->document($subaccount["vid"])->delete();
            $remove = $this->plaidService->removeItem($subaccount["account_id"]);
        }

        $transactions = $db->collection("PlaidTransactions")->document($subaccount["vid"])->delete();

        $remove = $this->plaidService->removeItem($data["accessToken"]);
    }

    public function getBankSubaccountBalance(Request $request){
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $subaccount = $db->collection("PlaidSubaccounts")->document($request->vid)->snapshot()->data();

        // get the balance for the account
        $startDate = new DateTime(Carbon::now()->subDays(30)->format('Y-m-d')); // Transactions from the past 30 days
        $endDate = new DateTime(Carbon::now()->format('Y-m-d'));

        $item = $this->plaidService->getItem($subaccount["access_token"]);
        $balances = (array) $this->plaidService->getTransactions($subaccount["access_token"], $startDate, $endDate, $subaccount["account_id"]);



        $balances = collect($balances["accounts"]);

        $balance = $balances->where("account_id","=",$subaccount["account_id"])->first();

        return json_encode($balance->balances ?? false);
    }

    public function updateBankTransactions(Request $request){
        // get all connected subaccounts 

        $subaccounts = $db->collection("PlaidSubaccounts")->documents();

        foreach($subaccounts as $subaccount){
            // 

            $subaccount = $subaccount->data();

            $transactions = $db->collection("PlaidTransactions")->where("account_id", "=", $subaccount["account_id"])->documents();

            $existingTransactions = [];

            foreach($transactions as $transaction){
                array_push($existingTransactions, $transaction->data());
            }

            // get the date of the first transaction

            $startDate = 

            $transactions = $this->plaidService->getTransactions($request->access_token, $startDate, $endDate, $request->account_id);
        
        }
    }
}
