<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Kreait\Firebase\Exception\FirebaseException;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

use App\Models\Transaction as Transaction;
use App\Models\User as User;

use Session;
use Location;
use Redirect;
use Mail;
use Exception;
use Validator;
use Dwolla;

use \Stripe\Stripe as Stripe;
use \Stripe\StripeClient as StripeClient;


class BoardingPassController extends Controller
{
    public function __construct(){
        $this->middleware('firebase.auth');
    }

    public function checkCustomerStatus(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));
        $dwolla = new Dwolla;

        $customer = $dwolla->getCustomer($user, $auth, $db);

        if(!$customer){
            return json_encode(500); // still pending
        }

        $customer = $dwolla->readCustomer($customer["customerID"]);

        $customerDoc = $db->collection("Customers")->document($customer->id);

        if($customer->status == "verified"){
            $update = $customerDoc->update([
                    ["path" => "status","value" => "verified"]
            ]);
        }

        // return customClaims
        $customClaims = $user->customClaims;
        $customClaims["status"] = $customer->status;

        return json_encode($customClaims);
    }

    public function createFundingSource(Request $request){
        // check inputs again
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));

        $dwolla = new Dwolla;
        $customer = $dwolla->getCustomer($user, $auth, $db);

        // we need to check if the bank's routing number has been black listed

        $blacklisted = isBankBlacklisted($request->routingNumber);

        if($blacklisted){
            // the customer tried to add a blacklisted bank account
            return json_encode([
                "error" => 500,
                "message" => "We are not allowing deposits and withdrawals with Idaho Central Credit Union at this time. Please use a different bank."
            ]);
        }

        // get an inventory of funding sources

        $fundingSources = $dwolla->fundingSources($customer["customerID"]);
        $fundingSourcesCount = 0;

        foreach($fundingSources as $fundingSource){
            if(!$fundingSource->removed 
                && isset($fundingSource->bankAccountType)
                && $fundingSource->status == "verified"
            ){
                $fundingSourcesCount++;
            }
            if(!$fundingSource->removed 
                && isset($fundingSource->bankAccountType)
                && $fundingSource->status == "unverified"
            ){
                $fundingSourcesCount++;
            }
        }

        // check the age of the account

        $createdAt = $user->metadata->createdAt->format("Y-m-d");

        $creationDate = new Carbon($createdAt);

        $accountAge = $creationDate->diffInDays(Carbon::now());

        if($fundingSourcesCount > 0 && $accountAge < 30){
            return json_encode([
                "error" => 500,
                "message" => "Accounts created within the last 30 days are limited to one funding source."
            ]);
        }


        
        $fundingSource = $dwolla->createFundingSource($customer, $request);

        

        if($fundingSource){
            // store funding source url with form details $fundingSource 

            # => "https://api-sandbox.dwolla.com/funding-sources/375c6781-2a17-476c-84f7-db7d2f6ffb31"


            $fundingSourceID = explode("/funding-sources/",$fundingSource)[1] ?? false;

            if($fundingSourceID){

                // db connection

                // initiate micro deposits

                $initiateMicrodeposits = $dwolla->initiateMicrodeposits($fundingSource);

                $fundingDoc = $db->collection("Sources")->document($fundingSourceID)->set([
                    "uid" => $user->uid,
                    "source" => $fundingSource,
                    "id" => $fundingSourceID,
                    "microdeposited" => $initiateMicrodeposits,
                    "name" => $request->name,
                    "owner" => cleanName($request->accountName),
                    "number" => $request->accountNumber
                ]);

            }
            else{
                return json_encode([
                    "error" => 500,
                    "message" => "A system error occured. Please contact venti support via admin@venti.co"
                ]);
            }

            return json_encode(200);
        }

        return json_encode([
            "error" => 500,
            "message" => "Your bank account details did not pass verification. Please try again or contact support via admin@venti.co"
        ]);
    }   

    public function removeFundingSource(Request $request){

        if(!isset($request->fundingSourceID)){
            return json_encode(500);
        }
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        if($request->sourceType == "bank"){
            $dwolla = new Dwolla;

            try{
                $fundingDoc = $db->collection("Sources")->document($request->fundingSourceID)->snapshot()->data();
                if($fundingDoc == null){
                    return json_encode(500);
                }
                $dwolla->removeFundingSource($fundingDoc["source"]);
                return json_encode(200); 
            }
            catch(FirebaseException $e){
                return json_encode(500); 
            }
        }
        if($request->sourceType == "card"){
            $user = $auth->getUser($request->headers->get('uid'));
            $card = $db->collection("Cards")->document($request->fundingSourceID)->snapshot()->data();

            if(!$card || $card["uid"] != $user->uid){
                return json_encode(500);
            }

            $delete = $db->collection("Cards")->document($request->fundingSourceID)->update([
                ["path" => "deletedAt","value" => Carbon::now('UTC')->timestamp]
            ]);

            return json_encode(200);
        }

        return json_encode(500);
    }

    public function withdrawToBank(Request $request){
        // lots of validation

        // I think withdraws should be instant because it can be difficult to actuate the balance

        // get the wallet balance


        $dwolla = new Dwolla;

        $validator = Validator::make($request->all(), [
            'withdrawAmount' => 'required|max:9',
            'withdrawSpeed' => 'required',
            'withdrawSource' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(500);
        }

        $withdrawAmount = $request->withdrawAmount;

        $fee = 0;

        if($request->withdrawSpeed == "next-available"){
            $fee = 2;
        }

        $total = round($withdrawAmount,0) - $fee;

        if($total <= 0){
            // the user entered an invalid value
            return json_encode(500);
        }

        // total represents the amount in their wallet minus the Venti fee
        

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));
        // make sure the withdraw amount does not exceed the wallet balance
        $customer = $dwolla->getCustomer($user, $auth, $db);
        $wallet = $dwolla->getWallet($customer["customerID"]);
        $walletDetails = $dwolla->getFundingSourceBalance($wallet->_links->self->href);

        $balance = (float) $walletDetails->balance->value;
        

        if($total > $balance){
            // the slick user is trying withdraw $10 (with fee) when the wallet only has $8
            return json_encode(501);
        }


        // get the bank URL

        $bank = $db->collection("Sources")->document($request->withdrawSource)->snapshot()->data();

        if($bank){

            if($customer){
                
                $bankUrl = $bank["source"];
                $transactionID = generateRandomString(12);
                $transfer = $dwolla->withdrawToBank($user, $wallet->_links->self->href, $bankUrl, $transactionID, $total, $request->withdrawSpeed, "Venti Withdraw: $transactionID");

                if($transfer){
                    // the transfer was successful
                    
                    $transaction = new Transaction;
                    $transaction->withdrawCash($transactionID, $transfer, $total, 0, $user, $bank["name"], $request->withdrawSpeed, "pending", "Venti Boarding Pass Withdraw");

                    if($fee != 0){
                        $feeWithdraw = $dwolla->transferFromUserWalletToVentiWallet($user, $wallet->_links->self->href, $fee, "Withdraw: $transactionID");

                        if($feeWithdraw){
                            $transaction->withdrawCash($transactionID, $feeWithdraw, $fee, 0, $user, "Venti Financial", "next-available", "pending", "Withdraw ACH Fee");
                        }   
                    }

                    return json_encode(200);
                }
                else {
                    return json_encode(500);
                }
            }
        }
    }

    public function depositToBoardingPass(Request $request){
        // lots of validation

        if($request->depositAmount > 7500){
            $error = [
                "code" => 500,
                "message" => 'Your request exceeds the $7,500 per transaction limit. Please lower the deposit amount and try again. Error code: 500.'
            ];
            return json_encode($error);  
        }

        if($request->depositSpeed != "standard" && $request->depositSpeed != "next-available"){
            $error = [
                "code" => 501,
                "message" => 'Your request has an invalid transaction speed. Please select "Standard" and try again. Error code: 501.'
            ];
            return json_encode($error);
        }

        $validator = Validator::make($request->all(), [
            'depositAmount' => 'required|min:1|max:9',
            'depositSpeed' => 'required',
            'depositSource' => 'required',
        ]);

        if ($validator->fails()) {
            $error = [
                "code" => 502,
                "message" => 'Your request has an invalid form data. Please try again. Error code: 502.'
            ];
            return json_encode($error);
        }

        // need to verify the deposit amount minus fees is not zero or below zero

        // store the deposit result into firebase

        $fee = 0;
        if($request->depositSpeed == "next-available"){
            $fee = 2;
        }

        $depositAmount = $request->depositAmount;
        $total = (float) $depositAmount - $fee;

        if($total - $fee <= 0){
            // this deposit will not add to the user wallet
            $error = [
                "code" => 503,
                "message" => 'Your request will not result in deposit due to fees. Please enter a higher deposit amount and try again. Error code: 503.'
            ];
            return json_encode($error);
        }

        // get the bank URL
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));

        $bank = $db->collection("Sources")->document($request->depositSource)->snapshot()->data();

        if($bank){
            $dwolla = new Dwolla;

            $customer = $dwolla->getCustomer($user, $auth, $db);
            if($customer){
                $wallet = $dwolla->getWallet($customer["customerID"]);
                // we need to ensure there is enough funds in the wallet to cover any fees

                
                $walletDetails = $dwolla->getFundingSourceBalance($wallet->_links->self->href);

                $balance = (float) $walletDetails->balance->value;

                if($fee > $balance){
                    // insufficient funds
                    $error = [
                        "code" => 504,
                        "message" => 'Due to fees, this transaction cannot be processed since it exceeds available funds. Please switch to "Standard" for transaction speed and try again. Error code: 504.'
                    ];
                    return json_encode($error);
                }

                $subscription = strtoupper($user->customClaims["subscription"] ?? "BUDDY");
                $depositCap = (int) env("VENTI_" . $subscription . "_DEPOSIT_CAP");

                if($balance >= $depositCap){
                    // reached deposit limit
                    $error = [
                        "code" => 505,
                        "message" => 'Your subscription is limited to a maximum Cash Balance of $' . $depositCap . '. You can lower your Cash Balance by purchasing travel services and Points. Error code: 505.'
                    ];
                    return json_encode($error); 
                }

                $bankUrl = $bank["source"];
                $transactionID = generateRandomString(12); // for customer reference only
                
                $transfer = $dwolla->depositToBoardingPass($user, $wallet->_links->self->href, $bankUrl, $transactionID, $total, $request->depositSpeed, "Deposit: $transactionID");

                if($transfer){
                    // the transfer from the bank was successful
                    // take the fee if there is one

                    $transaction = new Transaction;
                    $transaction->depositCash($transactionID, $transfer, $total, 0, $user, $bank["name"], $request->depositSpeed, "pending","Venti Boarding Pass Deposit", $request->depositEnv ?? "mobile");



                    if($fee != 0){
                        $feeWithdraw = $dwolla->transferFromUserWalletToVentiWallet($user, $wallet->_links->self->href, $fee, "Deposit: $transactionID");

                        $transactionID->withdrawCash($transactionID, $feeWithdraw, $fee, 0, $user, "Venti Financial", "next-available", "pending","Deposit ACH Fee");
                    }

                    // send the user an email at this step to confirm

                    return json_encode(200);
                }
                else {
                    // auto fail if unverified
                    $error = [
                        "code" => 506,
                        "message" => 'Our ACH partner, Dwolla, has denied this deposit request. Please try again or contact support via admin@venti.co. Error code: 506.'
                    ];
                    return json_encode($error);
                }
            }
        }

        $error = [
            "code" => 507,
            "message" => 'We do not have record of this bank account. Please contact support via admin@venti.co. Error code: 507'
        ];

        return json_encode($error);
    }

    public function depositPoints(Request $request){

        $validator = Validator::make($request->all(), [
            'depositAmount' => 'required',
            'transactionID' => 'required'
        ]);

        if ($validator->fails()) {
            $error = [
                "code" => 502,
                "message" => 'Your request has an invalid form data. Please try again. Error code: 502.'
            ];
            return json_encode($error);
        }

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));
        $fee = $request->fee ?? 0;
        $note = $request->note;

        $transaction = new Transaction;
        $transaction->depositPoints($request->transactionID, null, $request->depositAmount, $fee, $user, $note);

        $success = [
            "code" => 200,
            "message" => ''
        ];

        return json_encode($success);
    }

    public function withdrawPoints(Request $request){

        $validator = Validator::make($request->all(), [
            'withdrawAmount' => 'required',
            'transactionID' => 'required'
        ]);

        if ($validator->fails()) {
            $error = [
                "code" => 502,
                "message" => 'Your request has an invalid form data. Please try again. Error code: 502.'
            ];
            return json_encode($error);
        }

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));
        $fee = $request->fee ?? 0;
        $note = $request->note;

        $transaction = new Transaction;
        $transaction->withdrawPoints($request->transactionID, null, $request->withdrawAmount, $fee, $user, $note);

        $success = [
            "code" => 200,
            "message" => ''
        ];

        return json_encode($success);
    }

    public function verifyBank(Request $request){
        $firstDeposit = (int) $request->firstDeposit;
        $secondDeposit = (int) $request->secondDeposit;

        if(
            $firstDeposit > 20 || $firstDeposit < 1 ||
            $secondDeposit > 20 || $secondDeposit < 1
        ){
            // deposit amounts are invalid
            return json_encode(500);  
        }

        $validator = Validator::make($request->all(), [
            'firstDeposit' => 'required|min:1|max:2',
            'secondDeposit' => 'required|min:1|max:2',
            'fundingSourceID' => 'required',
        ]);

        if ($validator->fails()) {
            // failed validation

            return json_encode(500);
        }

        // get the bank URL
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $user = $auth->getUser($request->header('uid'));

        $bank = $db->collection("Sources")->document($request->fundingSourceID)->snapshot()->data();

        if($bank){
            $dwolla = new Dwolla;
            $verificationCheck =  $dwolla->verifyFundingSource($bank["source"], toPennies($firstDeposit), toPennies($secondDeposit));

            if($verificationCheck){
                return json_encode(200);
            } else{
                // 
                return json_encode(201);
            }
        }

        return json_encode(500);
    }

    public function bankDetails(Request $request){
        $fundingSourceUrl = env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))) . "/funding-sources/" . $request->source;

        $dwolla = new Dwolla;

        $fundingSource = $dwolla->getFundingSource($fundingSourceUrl);

        return json_encode($fundingSource);
    }

    public function getCards(Request $request){
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->header('uid'));
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $cards = [];

        $cardDocs = $db->collection("Cards")
            ->where("uid", "=", $user->uid)
            ->where("deletedAt","=", null)
            ->documents();

        foreach($cardDocs as $card){
            $card = $card->data();
            array_push($cards, $card);
        }

        return json_encode($cards);
    }

    public function saveCard(Request $request){
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->header('uid'));
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $validator = Validator::make($request->all(), [
            'payment_method_id' => 'required',
            'cardName' => 'nullable|string|max:40|regex:/^[a-zA-Z\s]+$/',
            'cardNickname' => 'nullable|string|max:30',
            'cardBrand' => 'nullable|string|max:30',
        ]);

        if ($validator->fails()) {
            return json_encode(["status" => 500, "message" => $validator->errors()->first()]);
        }

        // check if the user has a stripe customer record
        
        $stripeCustomer = \Stripe::checkCustomerExists($user->email);
        $name = getFirstName($user->displayName) . " " . getLastName($user->displayName);

        if(!$stripeCustomer){
            // create the customer
            $stripeCustomer  = \Stripe::createCustomer($name, $user->email, $user->customClaims["phoneNumber"]);
        }
        
        $saveCard = \Stripe::saveCard($stripeCustomer->id, $request->payment_method_id);

        if($saveCard === true){
            // save to firebase
            $firestore = app('firebase.firestore');
            $db = $firestore->database();

            $card = $db->collection("Cards")->document($request->payment_method_id)->set([
                "paymentId" => $request->payment_method_id,
                "uid" => $user->uid,
                "customerID"=>$stripeCustomer->id,
                "cardNickname" => $request->cardNickname,
                "cardName" => $request->cardName,
                "cardBrand" => ucfirst($request->cardBrand),
                "cardLast4" => $request->cardLast4,
                "cardExpMonth" => $request->cardExpMonth,
                "cardExpYear" => $request->cardExpYear,
                "cardType" => $request->cardType,
                "createdAt" => Carbon::now('UTC')->timestamp,
                "deletedAt" => null
            ]);

            return json_encode(["status" => 200,  "message" => "Card added successfully"]);
        }

        return json_encode(["status" => 500, "message" => $saveCard]);
    }


    public function getMarketingStatus(Request $request){
            $auth = app('firebase.auth');
            $user = $auth->getUser($request->headers->get('uid'));
            $memberID = md5($user->email);

            $url = "https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID";

            $result = Http::get($url, [
                'api_key' => env('EMAIL_OCTOPUS_API_KEY')
            ]);

            $result = json_decode($result->body(), true);

            $tags = [];

            if(array_key_exists("status", $result)){
                $tags = $result["tags"];
                if($result["status"] != "SUBSCRIBED"){
                    return false;
                }
            }

            $customClaims = $user->customClaims;
            $customClaims["name"] = getFullName($user);
            $customClaims["email"] = $user->email;
            $customClaims["tags"] = $tags;
            $customClaims["subscription"] = ucfirst($customClaims["subscription"] ?? "Buddy") . " Class";

            return json_encode($customClaims);
    }

    public function updateMarketingStatus(Request $request){
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));
        $memberID = md5($user->email);

        $url = "https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID";

        $tag = $request->tag;
        $enabled = true;
        if($request->enabled == "false"){
            $enabled = false;
        }

        $result = Http::put($url, [
            'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
            "tags" => [
                "$tag"  => $enabled,
            ]
        ]);

        json_encode(200);
    }

    public function sharePoints(Request $request){
        $email = $request->pointsRecipient;
        $amount = $request->pointsToSend;

        $fee = round($amount * .02,2);
        $amount = $amount - $fee; // we take a 2% fee

        // we need to check to see if the email belongs to a user

        $firestore = app('firebase.firestore');
        $auth = app('firebase.auth');

        $validator = Validator::make($request->all(), [
            'pointsToSend' => 'required',
            'pointsRecipient' => 'required',
        ]);

        if ($validator->fails() || $request->pointsToSend <= 0.1) {
            // not all required fields presented
            return json_encode(504);
        }

        try{
            $recipient = $auth->getUserByEmail($email);
            // check to see if the user has a plan)
            $customClaims = $recipient->customClaims;
            $subscription = $customClaims["subscription"] ?? "buddy";
            $subscriptionID = $customClaims["subscriptionID"] ?? false;

            if(!$subscriptionID){
                // the customer has not finished onboarding
                return json_encode(501);
            }

            $sender = $auth->getUser($request->headers->get('uid'));

            if($sender->uid == $recipient->uid){
                // can't send to yourself
                return json_encode(502);
            }

            $db = $firestore->database();

            

            // check the points balance of the recipient if they are buddy pass holder

            if($subscription == "buddy"){
                $firebaseUser = new User($recipient->uid);
                $points = $firebaseUser->getPoints();

                if($amount + $points > 250){
                    // the buddy pass holder will exceed their maximum balance
                    return json_encode(503);
                }
            }

            // check to make sure the points being sent does not exceed my points
            $firebaseUser = new User($sender->uid);
            $myPoints = $firebaseUser->getPoints();

            if($request->pointsToSend > $myPoints){
                return json_encode(504);
            }

            // process the transaction from my perspective
            $transactionCol = $db->collection("Transactions");
            $transactionID = generateRandomString(12);
            $transactionID2 = generateRandomString(12);

            $transaction = new Transaction;
            $transaction
                ->sendPoints($transactionID, $sender, $recipient, $amount, $fee, getFirstName($recipient->displayName), "Points Transfer: $transactionID")
                ->receivePoints($transactionID2, $sender, $recipient, $amount, $fee, getFirstName($sender->displayName), "Points Transfer: $transactionID", $transactionID);

            // email the recipient

            $subject = "[Boarding Pass] Your Received $amount Points";
            $title = "You've Got Points!";
            $email = $recipient->email;
            

            $data = [
                "title" => $title,
                "amount" => $amount,
                "description" => "You received $amount Points from " . getFirstName($sender->displayName) . " with confirmation ID: $transactionID2.
                <br><br>
                Points help you buy down the cost of flights, hotels, and more on the Venti platform by up to 99%. Use the link below to check your balance, make deposits, purchase points, and more."
            ];

             Mail::send('emails.transactions.admin-transact', array("data" => $data), function($message) use($email, $subject)
                {
                    $message
                        ->to($email)
                        ->from("no-reply@venti.co","Venti")
                        ->replyTo("no-reply@venti.co")
                        ->subject($subject);
            });

            return json_encode(200);

        }
        catch(FirebaseException $e){
            // the user does not exist

            return json_encode(500);
        }
    }

    public function purchasePoints(Request $request){
        // when the user purchases something, we log it here

        $firestore = app('firebase.firestore');
        $auth = app('firebase.auth');
        $db = $firestore->database();
        $user = $auth->getUser($request->headers->get('uid'));

        $validator = Validator::make($request->all(), [
            'warpAmount' => 'required',
            'termsWarp' => 'required',
        ]);

        if ($validator->fails() || $request->warpAmount < 1) {
            // not all required fields presented
            return json_encode(501);
        }

        $firebaseUser = new User($user->uid);
        $canPurchase = $firebaseUser->canPointsPurchasePoints();

        if(!$canPurchase){
            return json_encode([
                "error" => 500,
                "message" => "You are limited to one warp drive purchase per week"
            ]);
        }

        // get the user cash balance

        $balance = json_decode($this->getBalances($user),true);

        $cashBalance = $balance["balance"];
        $warpAmount = $request->warpAmount;

        if($warpAmount > ($cashBalance * 0.25)){
            // request exceeds the allowed amount
            return json_encode(500);
        }

        // time to process the transaction

        $pointsToAward = $warpAmount * 0.9; //
        $customerWallet = $balance["wallet"];
        $fee = round($warpAmount - $pointsToAward,2);

        $dwolla = new Dwolla;

        $transfer = $dwolla->transferFromUserWalletToVentiWallet($user, $customerWallet, $warpAmount, "points-purchase");

        if($transfer){
            // the transfer succeeded. Time to record to firebase
            
            $transactionID = generateRandomString(12);

            $transaction = new Transaction();
            $transaction
                ->withdrawCash($transactionID, $transfer, $warpAmount, 0, $user, "Venti","next-available", "complete", "Points Purchase")
                ->depositPoints(generateRandomString(12), $transfer, $pointsToAward, $fee, $user, "Points Purchase: $transactionID");

            // send receipt

            $email = $user->email;
            $subject = "[Venti Boarding Pass] Receipt for Points Purchase";
            
                try{
                    Mail::send('emails.transactions.points-receipt', array("data" => [
                        "amount" => $warpAmount,
                        "pointsToAward" => $pointsToAward
                    ]), function($message) use($email, $subject)
                    {
                            $message
                                ->to($email)
                                ->from("no-reply@venti.co","Venti")
                                ->replyTo("no-reply@venti.co")
                                ->bcc("admin@venti.co")
                                ->subject($subject);
                    });
                }
                catch(Exception $e){

                }
                
            return json_encode(200);
        }

        return json_encode(503);
    }

    public function getBalances($user){
        // returns balance, points, and spending power
        $dwolla = new Dwolla;

        // get the customer

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $wallet = "";
        $balance = 0;

        // make sure the withdraw amount does not exceed the wallet balance
        $customer = $dwolla->getCustomer($user, $auth, $db);

        if($customer == false){
            
            $data = [
                "wallet" => "",
                "balance" => 0,
                "points" => 0
            ];

            return json_encode($data);
        }

        if($customer["status"] == "verified"){

            $wallet = $dwolla->getWallet($customer["customerID"]);

            $walletDetails = $dwolla->getFundingSourceBalance($wallet->_links->self->href);

            $balance = (float) $walletDetails->balance->value;

            $wallet = $wallet->_links->self->href;

        }

        $firebaseUser = new User($user->uid);
        $points = $firebaseUser->getPoints();

        $data = [
            "wallet" => $wallet,
            "balance" => $balance,
            "points" => $points
        ];

        return json_encode($data);

    }

    public function mysterybox(Request $request)
    {
        return json_encode(['status' => 400, "message" => "Mysterybox is unavailable at this time."]);


        $auth = app('firebase.auth');

        try{
            $user = $auth->getUser($request->headers->get('uid'));
        }
        catch(FirebaseException $e){
            return json_encode(['status' => 403, "message" => "User is invalid"]);
        }

        $validator = Validator::make($request->all(), [
            'wager' => 'required|min:1|max:5' // wager must be between 1 and 5
        ]);

        $wager = $request->wager;
        $env = $request->env ?? "mobile";

        if(!$validator){
            return json_encode(['status' => 402, "message" => "Data is invalid"]);
        }

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $totalPlays = json_decode($this->attemptsRemaining($user, $db), true);

        if($totalPlays["opens24Hours"] >= env('MYSTERY_LIMIT') && env('APP_ENV') == "production"){
            return json_encode(['status' => 403, "message" => "You can only open the box three times within a 24-hour period."]);
        }

        try{
            $user = $auth->getUser($user->uid);

            if($user->disabled){
                return json_encode(['status' => 404, "message" => "User disabled"]);
            }
        }
        catch(FirebaseException $e){
            if($user->disabled){
                return json_encode(['status' => 404, "message" => "User not found"]);
            }
        }

        // we need to check the points balance of the customer

        $transactions = [];

        $transactionDocs = $db->collection("Transactions");
        $transactionDocs = $transactionDocs
            ->where("uid","=",$user->uid)
            ->documents();

        foreach($transactionDocs as $transactionDoc){
            array_push($transactions,  $transactionDoc->data());
        }

        $balance = getPointsBalance($transactions);

        if($wager > $balance || $balance == 0){
            return json_encode(['status' => 405, "message" => "Insufficient points balance"]);
        }

        // by the time they get this far, they have permission to open the box

        // deduct the play from the user balance

        if($totalPlays["totalOpens"] > 0){

        } else {

        }

        $transactionID2 = generateRandomString(12);

        $transaction = new Transaction;
        $transaction->withdrawPoints($transactionID2 , null, $wager, 0, $user, "Mystery Box", $env);

        $price = ["prize" => 0, "odds" => 0];

        try{
            $prize = $this->openMysteryBox($wager);
        }
        catch(Exception $e){
            return json_encode(['status' => 406, "message" => "There was an error processing your request. Please contact support"]);
        }

        // points deducted. box opened. record result

        $data = [
            "transactionID" => $transactionID2,
            "prize" => $prize,
            "uid" => $user->uid,
            "type" => "points-withdraw",
            "timestamp" => Carbon::now('UTC')->timestamp, // UTC
            "date" => Carbon::now('UTC')->format("Y-m-d H:i:s"),
            "amount" => $wager,
            "odds" => $prize["odds"],
            "env" => env('APP_ENV')
        ];

        $result = $db->collection("MysteryBoxOpens")->document($transactionID2)->set($data);

        // record the opening of the box

        if(!$prize || $prize['prize'] == 0){
            // player has lost this round
            return json_encode([
                'status' => 201,
                "message" => "You didn't win anything"
            ]);
        }

        if($prize['prize'] == $wager || $prize["tier"] == "10xWin" || $prize["tier"] == "quintupleWin" || $prize["tier"] == "doubleWin"){
            $transactionID3 = generateRandomString(12);
            usleep(501); // prevents duplicate timestamps 
            $transaction->depositPoints($transactionID3, null, $prize['prize'], 0, $user, "Mystery Box: $transactionID2", $env);

            $net = $prize['prize'] - $wager;

            return json_encode([
                'status' => 200,
                "message" => "You won " . $prize["prize"] . " Points. A net gain of $net Points"
            ]);

        } else{
            // they won a big prize

            

            $tier = ucfirst($prize['tier']);

            $email = $user->email;

            $subject = "[Venti Boarding Pass] Your $tier Tier Prize Confirmation Email";
            
            try{
                Mail::send('emails.transactions.mysterybox', array("data" => [
                    "tier" => $tier,
                    "confirmation" => $transactionID2,
                    "date" => Carbon::now('UTC')->format("M. d, Y")
                ]), function($message) use($email, $subject)
                {
                        $message
                            ->to($email)
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->bcc("admin@venti.co")
                            ->subject($subject);
                });
            }
            catch(Exception $e){

            }

            return json_encode([
                'status' => 200,
                "message" => "You won a top-tier prize: $tier. We sent you a confirmation email and will be reaching out shortly."
            ]);
        }
    }

    public function openMysterybox($betAmount){
        

        // Define the odds, including win back, double, triple, and quintuple win odds
        $odds = [];
        for($i = 1; $i <= 5; $i+=2){
            $oddsAmount = 500000;
            if($i == 5){
                $oddsAmount = 625000;
            }

            $odds[$i] = [
                'dragon' => $oddsAmount / $i, // 20000
                'captain' => ($oddsAmount / ($i*(2**1))), // 10000
                'hightide' => ($oddsAmount/ ($i*(2**2))), // 5000
                'lowtide' => ($oddsAmount / ($i*(2**3))), // 2500
                'gold' => ($oddsAmount / ($i*2**4)), // 2000
                'silver' => ($oddsAmount / ($i*2**5)), // 1000
                'bronze' => ($oddsAmount / ($i*2**6)), // 500
                'maincabin' => ($oddsAmount / ($i*2**7)), // 250
                'tailwind' => ($oddsAmount / ($i*2**8)), // 150
                'tenXOdds' => $i == 5 ? 160 : ($i == 3 ? 192 : 256), // 50
                'quintupleWinOdds' => $i == 5 ? 40 : ($i == 3 ? 48 : 64),
                'doubleWinOdds' => $i == 5 ? 10 : ($i == 3 ? 12 : 16),
                'winBackOdds' => $i == 5 ? 2.25 : ($i == 3 ? 3 : 4)
            ];
        }

        $dragonPrize = 20000; // Grand Prize amount
        $captainPrize = 10000;
        $hightidePrize = 5000;
        $lowtidePrize = 2500;
        $goldPrize = 2000;
        $silverPrize = 1000;
        $bronzePrize = 500;
        $maincabinPrize = 250;
        $tailwindPrize = 200;

        // Validate the bet amount
        if (!isset($odds[$betAmount])) {
            return "Invalid bet amount. Please bet $1, $3, or $5.";
        }

        $dragon = mt_rand(1, $odds[$betAmount]['dragon']);
        $captain = mt_rand(1, $odds[$betAmount]['captain']);
        $hightide = mt_rand(1, $odds[$betAmount]['hightide']);
        $lowtide = mt_rand(1, $odds[$betAmount]['lowtide']);
        $gold = mt_rand(1, $odds[$betAmount]['gold']);
        $silver = mt_rand(1, $odds[$betAmount]['silver']);
        $bronze = mt_rand(1, $odds[$betAmount]['bronze']);
        $maincabin = mt_rand(1, $odds[$betAmount]['maincabin']);
        $tailwind = mt_rand(1, $odds[$betAmount]['tailwind']);
        $tenXOdds = mt_rand(1, $odds[$betAmount]['tenXOdds']);
        $quintupleWinOdds = mt_rand(1, $odds[$betAmount]['quintupleWinOdds']);
        $doubleWinOdds = mt_rand(1, $odds[$betAmount]['doubleWinOdds']);
        $winBackOdds = mt_rand(1, $odds[$betAmount]['winBackOdds']);

        if($doubleWinOdds == 1){
                return ["tier" => "doubleWin", "prize" => $betAmount * 2, "odds" => $odds[$betAmount]["doubleWinOdds"]];
        }
        
        if($dragon == 1){
                return ["tier" => "dragon", "prize" => $dragonPrize, "odds" => 100/$odds[$betAmount]["dragon"]];
        }
        if($captain == 1){
                return ["tier" => "captain", "prize" => $captainPrize, "odds" => 100/$odds[$betAmount]["captain"]];
        }
        if($hightide == 1){
                return ["tier" => "High Tide", "prize" => $hightidePrize, "odds" => 100/$odds[$betAmount]["hightide"]];
        }
        if($lowtide == 1){
                return ["tier" => "Low Tide", "prize" => $lowtidePrize, "odds" => 100/$odds[$betAmount]["lowtide"]];
        }
        if($gold == 1){
                return ["tier" => "gold", "prize" => $goldPrize, "odds" => 100/$odds[$betAmount]["gold"]];
        }
        if($silver == 1){
                return ["tier" => "silver", "prize" => $silverPrize, "odds" => 100/$odds[$betAmount]["silver"]];
        }
        if($bronze == 1){
                return ["tier" => "bronze", "prize" => $bronzePrize, "odds" => 100/$odds[$betAmount]["bronze"]];
        }
        if($maincabin == 1){
                return ["tier" => "Main Cabin", "prize" => $maincabinPrize, "odds" => 100/$odds[$betAmount]["maincabin"]];
        }
        if($tailwind == 1){
                return ["tier" => "Economy Plus", "prize" => $tailwindPrize, "odds" => 100/$odds[$betAmount]["tailwind"]];
        }
        if($tenXOdds == 1){
                return ["tier" => "10xWin", "prize" => $betAmount * 10, "odds" => $odds[$betAmount]["tenXOdds"]];
        }
        if($quintupleWinOdds == 1){
                return ["tier" => "quintupleWin", "prize" => $betAmount * 5, "odds" => $odds[$betAmount]["quintupleWinOdds"]];
        }
        
        if($winBackOdds == 1){
                return ["tier" => "winBackOdds", "prize" => $betAmount, "odds" => $odds[$betAmount]["winBackOdds"]];
        }

        return ["tier" => 0, "prize" => 0, "odds" => [1/$odds[$betAmount]["winBackOdds"], "1:" . $odds[$betAmount]["winBackOdds"]]]; // No win
    }

    public function opens(Request $request){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $user = $auth->getUser($request->headers->get('uid'));

        return $this->attemptsRemaining($user, $db);
    }

    public function attemptsRemaining($user = null, $db = null){
        // we want to limit the number of shakes to three times per day
        $last24Hours = Carbon::now('UTC')->subHours(24)->timestamp;
        $opens = [];
        $past24 = [];

        $openDocs = $db->collection("MysteryBoxOpens");
        $openDocs = $openDocs
            ->where("uid","=",$user->uid)
            ->documents();
        // I need a separate firebase instance for sandboxing

        foreach($openDocs as $openDoc){
            $data = $openDoc->data();

            array_push($opens,  $data);

            if($data["timestamp"] >= $last24Hours){
                array_push($past24, $data);
            }

        }

        return json_encode(["totalOpens" => sizeof($opens), "opens24Hours" => sizeof($past24)]);

    }

    public function superpowers(Request $request){
        $auth = app('firebase.auth');

        try{
            $user = $auth->getUser($request->headers->get('uid'));
        }
        catch(FirebaseException $e){
            return json_encode(['status' => 403, "message" => "User is invalid"]);
        }

        // get all superpowers held by a user

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $superpowerDocs = $db->collection("Superpowers");
        $superpowerDocs = $superpowerDocs
        ->where("uid","=",$user->uid)
        ->where("activated","=",false)
        ->documents();

        $superpowers = [];

        foreach($superpowerDocs as $superpower){
            array_push($superpowers, $superpower->data());
        }

        return json_encode($superpowers);
    }

    public function superpowerActivate(Request $request){
        $auth = app('firebase.auth');

        try{
            $user = $auth->getUser($request->headers->get('uid'));
        }
        catch(FirebaseException $e){
            return json_encode(['status' => 403, "message" => "User is invalid"]);
        }

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $superpowerDoc = $db->collection("Superpowers")->document($request->id);
        $superpower = $superpowerDoc->snapshot()->data();
        // activate this

        if(!$superpower){
            return json_encode(["status" => 400, "Superpower not found"]);
        }

        if($superpower["activated"] == true){
            return json_encode(["status" => 201, "This has already been used"]);
        }

        $update = $superpowerDoc->update([
            ["path" => "activated","value" => true],
            ["path" => "activatedOn","value" => Carbon::now('UTC')->timestamp],
        ]);
        $reward = 0;
        $tier = null;

        if($superpower["item"] == "mysterybox"){
            for($i = 1; $i <= $superpower["quantity"]; $i++){
                $result = $this->openMysterybox(1);

                  if($result["prize"] > 0 && $result["prize"] <= 10){
                    $reward += $result["prize"];
                  }
                  if($result["prize"] > 10){
                    $tier = $result["tier"];
                    break;
                  }
            }
        }

        return ["tier" => $tier, "prize" => $reward, "odds" => null];
    }

    public function pointsBalance(Request $request){
        $auth = app('firebase.auth');

        try{
            $user = $auth->getUser($request->headers->get('uid'));
        }
        catch(FirebaseException $e){
            return json_encode(['status' => 403, "message" => "User is invalid"]);
        }

        $firebaseUser = new User($user->uid);
        $points = $firebaseUser->getPoints();

        return json_encode($points);
    }

}
