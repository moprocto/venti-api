<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon as Carbon;

class NotificationsController extends Controller
{
    public function send(){
        $channelName = 'general';
        $recipient= 'ExponentPushToken[CxDM1KHOWxWCQbdePCPfH0]';
        
        // You can quickly bootup an expo instance
        $expo = \ExponentPhpSDK\Expo::normalSetup();
        
        // Subscribe the recipient to the server
        $expo->subscribe($channelName, $recipient);
        
        // Build the notification data
        $notification = ['body' => 'Hello World!'];
        
        // Notify an interest with a notification
        $expo->notify([$channelName], $notification);
    }

    public function setPushNotifications(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));
        $doc = $db->collection("NotificationTokens")->document($user->uid);
        $data = $doc->snapshot()->data();

        $tokens = $data["tokens"] ?? [];
        $token = [
            $request->token => [
                "uid" => $user->uid,
                "deviceName" => $request->deviceName,
                "deviceType" => $request->deviceType,
                "token" => $request->token
            ]
        ];

        if(sizeof($tokens) > 0){
            if(!array_key_exists($request->token, $tokens)){
                $tokens[$request->token] = [
                    "uid" => $user->uid,
                    "deviceName" => $request->deviceName,
                    "deviceType" => $request->deviceType,
                    "token" => $request->token
                ];
            } else{
                if($request->reactivate == true){
                    $tokens[$request->token]["deletedAt"] = null;
                }
            }

            $update = $doc->update([
                ["path" => "tokens","value" => $tokens]
            ]);
        }
        else {
            $doc->set(["tokens" => $token]);
        }
        

        return json_encode(200);
    }

    public function readPushNotification(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));
        $doc = $db->collection("NotificationTokens")->document($user->uid);
        $data = $doc->snapshot()->data();

        if(!isset($data)){
            return json_encode(404);
        }

        $tokens = $data["tokens"] ?? [];

        foreach($tokens as $token){
            if(array_key_exists("token", $token) && $token["token"] == $request->token){
                $deletedAt = $token["deletedAt"] ?? false;
                if($deletedAt && $deletedAt != null){
                    // token found but notifications disabled
                    return json_encode(404);
                } else {
                    return json_encode(200);
                }
            }
        }      

        return json_encode(403);  
    }

    public function deletePushNotification(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->headers->get('uid'));
        $doc = $db->collection("NotificationTokens")->document($user->uid);
        $data = $doc->snapshot()->data();

        $tokens = $data["tokens"] ?? [];

        if($tokens){
            foreach($tokens as $key => $value){
                if($value){
                }
            }
            if(array_key_exists($request->token, $tokens)){
                $tokens[$request->token]["deletedAt"] = Carbon::now('UTC')->timestamp;
            }

            if(isset($data)){
                $update = $doc->update([
                    ["path" => "tokens","value" => $tokens]
                ]);
            }
        }

        return json_encode(200);
    }
}
