<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Dwolla;
use Validator;
use Redirect;
use Mail;
use Carbon\Carbon as Carbon;


class TripsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('firebase.auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request){
        // get all orders for the user

        $dwolla = new Dwolla;

        // get the customer

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->header('uid'));

        $orderDocs = $db->collection("Orders")
            ->where("uid","=", $user->uid)
            ->documents();


        $flights = [];
        $hotels = [];
        $orders = 0;

        foreach($orderDocs as $order){
            $order = $order->data();

            $orderData = json_decode($order["order"], true);
            $order["order"] = $orderData["data"] ?? $orderData;
            if($order["type"] == "flight"){
                
                $duffelOrder = $order["order"];
                $owner = $duffelOrder["owner"];
                $slices = $duffelOrder["slices"];

                $originData = getTripOriginData($slices);

                $order["destination"] = $originData["destination"]["name"];

                $lastSlice = sizeof($slices) - 1;
                $firstSlice = $slices[0];
                $lastSlice = $slices[$lastSlice];

                $order["origin"] = $firstSlice["origin"]["iata_code"] ?? $firstSlice["origin"]["iata_city_code"];
                $firstSegment = $firstSlice["segments"][0] ?? $firstSlice;
                $order["image"] = $firstSegment["marketing_carrier"]["logo_symbol_url"];
                $departingAt = \Carbon\Carbon::parse($firstSegment["departing_at"])->format("g:i A");
                $order["month"] = strtoupper(substr(\Carbon\Carbon::parse($firstSegment["departing_at"])->format("F"),0,3));
                $order["departureStamp"] = \Carbon\Carbon::parse($firstSegment["departing_at"])->timestamp;
                $order["day"] = \Carbon\Carbon::parse($firstSegment["departing_at"])->format("d");
                $order["description"] = $departingAt . " " . $firstSegment["marketing_carrier"]["name"] . " flight to " . $originData["destination"]["city_name"] . " arriving at:";
                array_push($flights, $order);


            }
            if($order["type"] == "hotel"){
                $photo = false;
                    $photos = $order["order"]["accommodation"]["photos"] ?? [];
                    if(sizeof($photos)>0){
                        $photo = $photos[0]["url"];
                    }
                $order["image"] = $photo;
                $order["title"] = $order["order"]["accommodation"]["name"];

                $order["day"] = \Carbon\Carbon::parse($order["order"]["check_in_date"])->format("d");
                $order["month"] = strtoupper(substr(\Carbon\Carbon::parse($order["order"]["check_in_date"])->format("F"),0,3));
                $order["checkIn"] = \Carbon\Carbon::parse($order["order"]["check_in_date"])->format("F d, Y");
                $order["checkOut"] = \Carbon\Carbon::parse($order["order"]["check_out_date"])->format("F d, Y");
                $order["guests"] = sizeof($order["order"]["guests"]);
                $order['rooms'] = $order["order"]["rooms"];
                array_push($hotels, $order);
                $orders++;
            }
            
        }

        if(sizeof($flights) > 0){
            usort($flights, function($a, $b) {
                return $b['departureStamp'] <=> $a['departureStamp'];
            });
        }   
        
        if(sizeof($hotels) > 0){
            usort($hotels, function($a, $b) {
                return $b['timestamp'] <=> $a['timestamp'];
            });
        }

        // calc points

        $points = $this->getPoints($user, $db);

        // get trackers

        $trackers = [];

        $trackersList = $db->collection("Trackers")->where("uid","=", $user->uid)
            ->documents();

        foreach($trackersList as $tracker){
            $tracker = $tracker->data();
            array_push($trackers, $tracker);
        }

        $data = [
            "title" => "My Trips",
            "flights" => $flights,
            "hotels" => $hotels,
            "orders" => $orders,
            "user" => $user,
            "points" => $points,
            "trackedFlights" => $trackers
        ];

        return json_encode($data);
    }

    public function tripDetailFlight(Request $request){
        $user = $request->headers->get('uid');
        $order = $request->headers->get('refID');

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($user);

        $order = $db->collection("Orders")->document($order)->snapshot()->data();

        // check permission of user to get details about it

        if($order["uid"] != $user->uid){
            // permission denied
            if(!isAdmin($user)){
                return json_encode(501);
            }   
        }

        $type = $order["type"];

        $url = "https://api.duffel.com/air/orders/" . $order["refID"];

        // query duffel to get the latest data about the order

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);
        curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result,true)["data"] ?? false;

        $user = $auth->getUser($order["uid"]);

        // report the status back to the user

        $originData = getTripOriginData($result["slices"]);
        $changeable = getTripCancelationDetails($result["conditions"]);
        $cancelable = getTripChangeDetails($result["conditions"]);
        $order["bagUpgrades"] = $order["bagUpgrades"] ?? [];
        $order["seatUpgrades"] = $order["seatUpgrades"] ?? [];
        $purchaseData = json_decode($order["order"], true);

        $airline_initiated_changes = $result["airline_initiated_changes"] ?? false;

        $data = [
            "title" => "Flight to " . $originData["destination"]["city_name"] . ": " . $order["booking_reference"],
            "duffelData" => $result,
            "order" => $order,
            "originData" => $originData,
            "user" => $user,
            "changeable" => $changeable,
            "cancelable" => $cancelable,
            "airline_initiated_changes" => $airline_initiated_changes,
            "type" => "flight"
        ];

        return json_encode($data);
    }

    public function tripDetailHotel(Request $request){
        $user = $request->headers->get('uid');
        $order = $request->headers->get('refID');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($user);

        $order = $db->collection("Orders")->document($order)->snapshot()->data();

        // check permission of user to get details about it

        if($order["uid"] != $user->uid){
            // permission denied
            if(!isAdmin($user)){
                return json_encode(501);
            }   
        }

        $type = $order["type"];

        $url ="https://api.duffel.com/stays/bookings/" . $order["refID"];

        // query duffel to get the latest data about the order

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);
        curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result,true)["data"] ?? false;

        $user = $auth->getUser($order["uid"]);

        // report the status back to the user

        $hotel = $result["accommodation"];

        $cancelable = $result["accommodation"]["rooms"][0]["rates"][0]["cancellation_timeline"];

        $photo = null;

        $ref = $order["booking_reference"];

        $order = json_decode($order["order"], true);

        if(isset($order["accommodation"]["photos"][0])){
            $photo = $order["accommodation"]["photos"][0]["url"];
        }

        $data = [
            "title" => "My Reservation: ". $ref,
            "duffelData" => $order,
            "order" => $result,
            "user" => $user,
            "changeable" => false,
            "cancelable" => $cancelable,
            "canceledOn" => false,
            "type" => "hotel",
            "adults" => $result["guests"],
            "photo" => $photo,
            "booking_reference" => $ref
        ];

        return json_encode($data);
    }

    public function getPoints($user, $db){
        $transactions = [];

        $transactionDocs = $db->collection("Transactions");
        $transactionDocs = $transactionDocs
                ->where("uid","=",$user->uid)
                ->where("env","=",env("APP_ENV"))
                ->documents();
            // I need a separate firebase instance for sandboxing

            foreach($transactionDocs as $transactionDoc){
                array_push($transactions,  $transactionDoc->data());
            }

        return getPointsBalance($transactions);
    }


}
