<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Storage;

class PlaidTransactionSycn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:plaid_transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will update customer transaction data';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // pull all events from Firebase
        

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $ideas = $db->collection('Ideas');
        $ideas = $ideas->where("archived","=", 0)->documents();
        $ideaList = [];

        $user = $auth->getUser("mnTmABt3MDOWyy76pwG7V0QACpD2");

        foreach($ideas as $idea){
            $data = $idea->data();

            if(array_key_exists("clientID", $data)
                && array_key_exists("group", $data)
                && $data["clientID"] != "mnTmABt3MDOWyy76pwG7V0QACpD2"
            ){
                try{
                    if (isset($data["privacy"]) && ($data["privacy"] != "full") && $data["privacy"] != "Invitation Only. Invisible to Others") {
                        //make sure it is visible to the public

                        $isTravel = true;

                        if(array_key_exists("type", $data) && strtolower($data["type"]) == "activity"){
                            $isTravel = false;
                            $daysRemaining = false;
                            $checkOut = null;
                        }

                        
                        if($isTravel){
                            //make sure it's not in the past
                            $departure = Carbon::createFromFormat('Y-m-d', str_replace("/","-",$data["departure"]))->timezone('America/New_York');
                            $today = Carbon::now()->timezone('America/New_York');
                            
                            $checkOut = Carbon::createFromFormat('Y-m-d', str_replace("/","-",$data["departure"]))->timezone('America/New_York')->addDays($data["days"]);

                            $daysRemaining = $departure->isPast();

                            $data["monthDepart"] = $departure->format("F");
                        }

                        if($daysRemaining == false){
                            $data["daysRemaining"] = $daysRemaining;

                            try{
                                if($user->uid != $data["clientID"]){
                                    
                                }



                                $author = $auth->getUser($data["clientID"]);


                                if($author->disabled == true){
                                    continue;
                                }

                                $data["owner"] = $data["clientID"];
                                $data["checkOut"] = $checkOut;
                                $data["author"] = (array) $author;
                                $data["groupSize"] = getGroupSize($data["group"]);
                                $data["groupChemistry"] = 0;
                                $data["isTravel"] = $isTravel;
                                // get travel group avatars

                                $groupMembers = $data["group"];
                                $groupAvatars = [];


                                $groupMembers = organizeMembersList($groupMembers);

                                for($i = 0; $i < 3; $i++){
                                    if(array_key_exists($i, $groupMembers)){
                                        if($groupMembers[$i]["role"] == "viewer"){
                                            
                                            if($user->uid == $groupMembers[$i]["uid"]){
                                                continue;
                                                // I'm already in this group
                                            }
                                            $member = $auth->getUser($groupMembers[$i]["uid"]);
                                            array_push($groupAvatars, $member->photoUrl);
                                        }
                                    }
                                }
                                $data["groupAvatars"] = $groupAvatars;

                                // here we calculate group chemistry

                                if(!is_null($user)){
                                    if($data["clientID"] != $user->uid){
                                        $groupChemistry = null;
                                    }
                                }
                                

                                array_push($ideaList, $data);
                            }
                            catch(FirebaseException $e){

                            }
                        }
                    }
                }
                catch(FirebaseException $e){

                }
            }
        }

        Storage::disk('cache')->put('search.json', json_encode($ideaList));


        return Command::SUCCESS;
    }
}
