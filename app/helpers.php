<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

function isAdmin($user){
    $admins = explode(",",env('APP_ADMINS_' . strtoupper(env('APP_ENV'))));

    if(in_array($user->uid, $admins)){
        // the user is an admin
        return true;
    }
}

function getTripLocation($destination, $country){
    if($destination == $country){
        return $destination;
    }

    return "$destination, $country";
}

function getGroupSize($groupMembers){
    $size = 0;

    foreach($groupMembers as $groupMember){
        if($groupMember["role"] == "viewer" || $groupMember["role"] == "owner"){
            $size++;
        }
    }

    return $size;
}


function getFirstName($full_name){
    $name = explode("|", $full_name);
    if(!str_contains($full_name, "|")){
        $name = explode(" ", $full_name);
    }
    $name = $name[0];
    return $name;
}

function getLastName($full_name){
    $name = explode("|", $full_name);
    if(!str_contains($full_name, "|")){
        $name = explode(" ", $full_name);
    }
    $name = $name[1] ?? "";
    return $name;
}

function getFullName($user){
    $name = explode("|", $user->displayName);
    if(array_key_exists("middleName", $user->customClaims)){
        $name = $name[0] + " " + $user->customClaims["middleName"] + " " + $name[1];
    } else {
        $name = str_replace("|"," ",$user->displayName);
    }
    
    return $name;
}

function cleanName($full_name){
    $name = str_replace("|"," ",$full_name);
    return $name;
}

function toPennies($integer){
    return ($integer < 10) ? "0.0$integer" : "0.$integer";
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getPhoto($photoUrl){
    if($photoUrl == "" | $photoUrl == null || $photoUrl == "/assets/img/profile.jpg" || $photoUrl == "/assets/img/profile-2.jpg"){
        return "https://venti.co" . $photoUrl;
    }
    return $photoUrl;
}

    function isProfileComplete($user){
        if(array_key_exists("dob", $user->customClaims)){
            if($user->customClaims["dob"] == "" || calculateAge($user->customClaims["dob"]) < 18){
                return [false, "Venti users must be at least 18 years of age", "Venti users must be at least 18 years of age. Contact admin@venti.co"];
            }
        } else{
            return [false,'Add your date of birth to your <a href="/user/profile">your profile</a>', 'Missing Date of Birth. Contact admin@venti.co.'];
        }

        if($user->photoUrl == "" || $user->photoUrl == "/assets/img/profile.jpg" || $user->photoUrl == "/assets/img/profile-2.jpg"){
            return [false, 'Add a headshot to <br><a href="/user/profile">your profile</a>', 'Add a Profile Picture'];
        }

        if(!$user->emailVerified){
            return [false,'<a href="/user/profile">Verify your email</a>', 'Verify Your Email'];
        }

        return [true, "", ""];
    }

    function calculateAge($dob){
        return Carbon::parse($dob)->diff(Carbon::now())->format('%y');
    }

    

    function isNumberBetween($init, $range, $test){
        $upper = $init + $range;
        $lower = $init - $range;

        if($test >= $lower && $test <= $upper){
            return true;
        }

        return false;
    }

    function getBackgroundImage($destination, $country){

        $name = getTripLocation($destination, $country);

        $details = Http::get("https://api.unsplash.com/search/photos?client_id=" . env('UNSPLASH_KEY'). "&per_page=10&query=" . $name . " landscape scenery")->getBody();

        $details = json_decode($details);


        if(isset($details->results)){
            foreach($details->results as $detail){
                return [
                    0 => $detail->urls->regular ?? "https://venti.co/assets/img/default-splash-bg.jpg",
                    1 => $detail->user->name ?? "Wendel Moretti",
                    2 => $detail->user->links->html ?? "https://www.pexels.com/@wendelmoretti"
                ];
            }
        } else {
            return [
                0 => "https://venti.co/assets/img/default-splash-bg.jpg",
                1 => "Wendel Moretti",
                2 => "https://www.pexels.com/@wendelmoretti"
            ];
        }
    }

    function getPointsBalance($transactions){
        $total = 0;
        foreach($transactions as $transaction){
            if($transaction["type"] == "points-deposit" || $transaction["type"] == "points-withdraw"){
                if(strtolower($transaction["status"]) == "complete"){
                    $amount = (float) $transaction["amount"];

                    if($transaction["type"] == "points-withdraw"){
                        $amount *= -1;
                    }

                    $total += $amount;
                }
            } 
        }

        return $total;
    }
    function getSweepstakeEntries($subscription, $cashBalance, $pointsBalance){
        $entries = 0;
        $entries += $cashBalance;

        switch ($subscription) {
            case 'buddy':
                $pointsBalance *= 1;
                break;
            case 'priority':
                $pointsBalance *= 2;
                break;
            case 'business':
                $pointsBalance *= 4;
                break;
            case 'first':
                $pointsBalance *= 8;
                break;
        }

        $entries += $pointsBalance;

        //16949.270675033 / 8 = 2,118.6588343791

        return round($entries, 0);
    }
    
function getTripOriginData($slices){
    // we need to communicate data about where the traveler is leaving from

    if(array_key_exists("segments", $slices[0])){
        $origin = $slices[0]["segments"][0];
    } else {
        $origin = $slices[0];
    }

    $data = [
        "origin" => $slices[0]["origin"],
        "destination" => $slices[0]["destination"],
        "details" => $origin
    ];

    return $data;
}

function getTripDestinationData($slices){
    // we need to communicate data about where the traveler is leaving from

    if(array_key_exists("segments", $slices[0])){
        $origin = $slices[0]["segments"][0];
    } else {
        $origin = $slices[0];
    }

    $data = [
        "departingOrigin" => $slices[0]["origin"],
        "destinationArrival" => $slices[0]["destination"],
        "originData" => $origin
    ];

    return $data;
}

function getTripCancelationDetails($conditions){
    if(is_null($conditions)){
        return false;
    }
    if(array_key_exists("refund_before_departure", $conditions)){
        if($conditions["refund_before_departure"] == null){
            return false;
        }
        if(array_key_exists("allowed", $conditions["refund_before_departure"])){
            if($conditions["refund_before_departure"]["allowed"] == true){
                // refund is allowed
                return [
                    "currency" => $conditions["refund_before_departure"]["penalty_currency"],
                    "amount" => $conditions["refund_before_departure"]["penalty_amount"]
                ];
            }
        }
    }
    // the trip cannot be canceled
    return false;
}

function getTripChangeDetails($conditions){
    if(is_null($conditions)){
        return false;
    }
    if(array_key_exists("change_before_departure", $conditions)){
        if($conditions["change_before_departure"] == null){
            return false;
        }
        if(array_key_exists("allowed", $conditions["change_before_departure"])){
            if($conditions["change_before_departure"]["allowed"] == true){
                // refund is allowed
                return [
                    "currency" => $conditions["change_before_departure"]["penalty_currency"],
                    "amount" => $conditions["change_before_departure"]["penalty_amount"]
                ];
            }
        }
    }
    // the trip cannot be canceled
    return false;
}

function searchUpgrades(array $availableUpgrades, $selectedItems, $typeToMatch) {
    $matchingUpgrades = [];

    // Loop through each selected item
    if($selectedItems == null){
        return [];
    }
    foreach ($selectedItems as $item) {
        // Make sure the 'id' key exists to prevent errors
        $itemId = isset($item['id']) ? $item['id'] : null;

        // Search for matching upgrades in the availableUpgrades array
        foreach ($availableUpgrades as $upgrade) {
            if ($upgrade['type'] === $typeToMatch && isset($upgrade['id']) && $upgrade['id'] === $itemId) {
                $price = $upgrade["total_amount"];
                $newPrice = round( baseMarkUpNonZero($price * getMarkupMultiplier($upgrade["type"] , $price)) ,0);
                $upgrade["newPrice"] = $newPrice;
                $matchingUpgrades[] = $upgrade;
                // get the markup value here

                break; // Break out of the inner loop once a match is found
            }
        }
    }

    return $matchingUpgrades;
}

function findRateById($rooms, $id) {
    foreach ($rooms as $room) {
        // If the current value is an array, recurse into it
        foreach($room["rates"] as $rate){
            // If the current key is 'id' and it matches the provided id, return the rate
            if($rate["id"] == $id){
                return $rate;
            }
        }
    }
    
    // Return null if nothing is found
    return null;
}

function compareSegmentTimes($data) {
    // Initialize an array to hold the comparison results
    $results = [];

    // Retrieve the 'removed' and 'added' segments from the changes
    $removedSegments = $data['changes']['removed'];
    $addedSegments = $data['changes']['added'];

    // Iterate over the removed segments
    foreach ($removedSegments as $index => $removed) {
        // Extract the corresponding added segment using the same index
        $added = $addedSegments[$index] ?? null;

        // Ensure there is a corresponding added segment
        if ($added) {
            // Compare each corresponding segment
            foreach ($removed['segments'] as $segIndex => $removedSegment) {
                $addedSegment = $added['segments'][$segIndex] ?? null;

                // Ensure the added segment exists
                if ($addedSegment) {
                    // Check for the existence of 'departureDatetime' and 'arrivalDatetime'
                    $removedDepTime =  $removedSegment['departure_datetime'] ?? $removedSegment['departureDatetime'];
                    $addedDepTime = $addedSegment['departure_datetime'] ?? $addedSegment['departureDatetime'];
                    $removedArrTime = $removedSegment['arrival_datetime'] ?? $removedSegment['arrivalDatetime'];
                    $addedArrTime = $addedSegment['arrival_datetime'] ?? $addedSegment['arrivalDatetime'];

                    // Check if there is a change in the departure or arrival datetime
                    $departureTimeChanged = ($removedDepTime && $addedDepTime) && ($removedDepTime !== $addedDepTime);
                    $arrivalTimeChanged = ($removedArrTime && $addedArrTime) && ($removedArrTime !== $addedArrTime);

                    // If there is a change, add it to the results
                    if ($departureTimeChanged || $arrivalTimeChanged) {
                        // Create segment label from origin to destination city names

                        $segmentLabel = ($removedSegment['origin']['city_name'] ?? '') . " to " . ($removedSegment['destination']['city_name'] ?? '');

                        $results[] = [
                            'segment_label' => $segmentLabel,
                            'removed_segment_id' => $removedSegment['id'],
                            'added_segment_id' => $addedSegment['id'],
                            'removed_departure_datetime' => $removedDepTime,
                            'added_departure_datetime' => $addedDepTime,
                            'removed_arrival_datetime' => $removedArrTime,
                            'added_arrival_datetime' => $addedArrTime,
                            'departure_time_changed' => $departureTimeChanged,
                            'arrival_time_changed' => $arrivalTimeChanged,
                        ];
                    }
                }
            }
        }
    }

    return $results;
}

function isBankBlacklisted($routing){
    $blocked = ["324173626"];

    if(in_array($routing, $blocked)){
        return true;
    }
    return false;
}