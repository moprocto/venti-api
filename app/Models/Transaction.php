<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Carbon\Carbon as Carbon;

use Dwolla;

final class Transaction extends Model
{
	private $firestore;
	private $db;

	public function __construct(){
		$this->firestore = app('firebase.firestore');
        $this->db = $this->firestore->database();
	}

    public function get($id, $user){
        $docs = $this->db->collection("Transactions");

        $transactionDocs = $docs->where("transactionID","=",$id)->documents();
        $data = null;

        foreach($transactionDocs as $transactionDoc){
            $data = $transactionDoc->data();
        }

        if(!$data){
            return null;
        }
        $timestamp = $data["timestamp"];

        $data["transactionID"] = explode("transfers/",$data['transactionUrl'])[1] ?? "";
        $data["timestamp"] = Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->format("M. d, Y");
        $data["ampm"] = Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->format("h:i A");
        $data["strtotime"] = Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->timestamp;
        $data["category"] = "Transfer";

        $type = "";
        $negative = false;

        if($data["type"] == "points-deposit" || $data["type"] == "points-withdraw"){
            $data["category"] = "Points";
        }

        if($data["type"] == "withdraw" || $data["type"] == "points-withdraw"){
            $negative = true;
        }

        $total = number_format($data["total"],2);

        if($data["category"] != "Points"){
            $type = "<br> Type: ACH " . ucfirst($data["type"]);
            $total = "$" . $total;
        }

        if($negative){
            // we show a negative sign for withdraw transactions
            $total = "-$total";
        }

        $data["status"] = ucfirst($data["status"]);
        $data["total"] = $total;

        // get days since creation

        $startDate = Carbon::createFromTimestampUTC($timestamp);
        $today = Carbon::now('UTC');

        if($today->diffInDays($startDate) < 2 && $data["status"] != "Complete"){
            $dwolla = new Dwolla;

            $transactionCancelLink = $dwolla->getCancel($data["transactionID"]) ?? false;

            if($transactionCancelLink){
                $transactionCancelLink = explode("transfers/", $transactionCancelLink)[1];
            }

            $data["transactionCancelLink"] = $transactionCancelLink;
        }

        return $data;
    }

	public function depositCash($id, $transfer, $amount, $fee, $user, $source, $speed, $status, $note, $env){
        $transferID = explode("transfers/",$transfer)[1];

        $data = [
            "transactionID" => $id,
            "uid" => $user->uid,
            "type" => "deposit",
            "timestamp" => Carbon::now('UTC')->timestamp, // UTC
            "date" => Carbon::now('UTC')->format("Y-m-d H:i:s"),
            "amount" => $amount,
            "speed" => $speed,
            "fee" => $fee,
            "from" => $source,
            "status" => "pending",
            "to" => "My Boarding Pass",
            "total" => $amount,
            "note" => $note,
            "transferID" => $transferID,
            "transactionUrl" => $transfer,
            "env" => $env ?? "mobile"
        ];

        $bank = $this->db->collection("Transactions")->document($transferID)->set($data);

        return $this;
	}

	public function withdrawCash($id, $transfer, $amount, $fee, $user, $destination, $speed, $status, $note){
		$transferID = explode("transfers/",$transfer)[1];
		$data = [
            "transactionID" => $id,
            "uid" => $user->uid,
            "type" => "withdraw",
            "timestamp" => Carbon::now('UTC')->timestamp, // UTC
            "date" => Carbon::now('UTC')->format("Y-m-d H:i:s"),
            "amount" => $amount,
            "speed" => $speed,
            "fee" => $fee,
            "status" => $status,
            "from" => "My Boarding Pass",
            "to" => $destination,
            "total" => $amount,
            "note" => $note,
            "transferID" => $transferID,
            "transactionUrl" => $transfer,
            "env" => "mobile"
        ];

        $result = $this->db->collection("Transactions")->document($transferID)->set($data);

        return $this;
	}

	public function depositPoints($id, $transfer = null, $amount, $fee, $user, $note, $env = "mobile"){
        $transferID = null;
        
        if($transfer){
            $transferID = explode("transfers/",$transfer)[1];
        }

		$data = [
            "transactionID" => $id,
            "uid" => $user->uid,
            "type" => "points-deposit",
            "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
            "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
            "amount" => $amount,
            "speed" => "next-available",
            "fee" => $fee,
            "status" => "complete",
            "from" => "Venti",
            "to" => "My Boarding Pass",
            "total" => $amount,
            "note" => $note,
            "transferID" => $transferID, // it's wise to link this with the previous transfer
            "transactionUrl" => null,
            "env" => $env
        ];

        $result = $this->db->collection("Transactions")->document($id)->set($data);

        return $this;
	}

    public function withdrawPoints($id, $transfer = null, $amount, $fee, $user, $note, $env = "mobile"){
        $transferID = null;

        if($transfer){
            $transferID = explode("transfers/",$transfer)[1];
        }

        $data = [
            "transactionID" => $id,
            "uid" => $user->uid,
            "type" => "points-withdraw",
            "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
            "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
            "amount" => $amount,
            "speed" => "next-available",
            "fee" => $fee,
            "status" => "complete",
            "from" => "My Boarding Pass",
            "to" => "Venti",
            "total" => $amount,
            "note" => $note,
            "transferID" => $transferID, // it's wise to link this with the previous transfer
            "transactionUrl" => null,
            "env" => $env
        ];

        $result = $this->db->collection("Transactions")->document($id)->set($data);

        return $this;
    }

    public function sendPoints($id, $sender, $recipient, $amount, $fee, $to, $note){

        $data = [
                "transactionID" => $id,
                "uid" => $sender->uid,
                "type" => "points-withdraw",
                "timestamp" => Carbon::now('UTC')->timestamp, // UTC
                "date" => Carbon::now('UTC')->format("Y-m-d H:i:s"),
                "amount" => $amount,
                "speed" => "next-available",
                "fee" => $fee,
                "status" => "complete",
                "from" => "My Boarding Pass",
                "to" => $to,
                "total" => $amount,
                "note" => $note,
                "transferID" => null,
                "transactionUrl" => null,
                "recipient" => $recipient->uid,
                "sender" => $sender->uid,
                "env" => "mobile",
                "taxable" => true // points are taxable when converted to cash equivalent value. In this case, the recipient is earning 
        ];

        $result = $this->db->collection("Transactions")->document($id)->set($data);

        return $this;
    }



    public function receivePoints($id, $sender, $recipient, $amount, $fee, $from, $note, $id2){
        $data = [
            "transactionID" => $id,
            "uid" => $recipient->uid,
            "type" => "points-deposit",
            "timestamp" => Carbon::now('UTC')->timestamp, // UTC
            "date" => Carbon::now('UTC')->format("Y-m-d H:i:s"),
            "amount" => $amount,
            "speed" => "next-available",
            "fee" => 0,
            "status" => "complete",
            "from" => $from,
            "to" => "My Boarding Pass",
            "total" => $amount,
            "note" => $note,
            "transferID" => $id2,
            "transactionUrl" => null,
            "recipient" => $recipient->uid,
            "sender" => $sender->uid,
            "env" => "mobile"
        ];

        $result = $this->db->collection("Transactions")->document($id)->set($data);

        return $this;
    }

    public function pointsBalance($user){

    }
}