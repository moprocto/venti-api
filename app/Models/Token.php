<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Carbon\Carbon as Carbon;

final class Token extends Model
{
	public $token;
	public $expiry;

	public function __construct($token = null, $expiry = null){
		// define attributes for existing token
		isset($token) ? ($this->token = $token) : null;
		isset($expiry) ? ($this->expiry = $expiry) : null;
	}

	public function createIfNew($auth, $user, $claims){
		if(isset($this->token)){
			return false;
		}
		$this->token = Str::random(60); // creates an x-api-token
        $this->expiry = Carbon::now('UTC')->addHours(1)->timestamp; // we will create a token object that expires in one hour
        $claims["api_token"] = $this->token;
		$claims["api_token_expiry"] = $this->expiry;
		$auth->setCustomUserClaims($user, $claims);
	}

	public function isExpired(): bool{
		$now = Carbon::now('UTC')->timestamp;
		if($this->expiry < $now){
			return true;
		}
		return false;
	}
}