<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Kreait\Firebase\Exception\FirebaseException;
use Carbon\Carbon as Carbon;
use Dwolla;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $auth;
    public $firebase;
    public $db;
    public $user;
    public $uid;
    public $transactions = [];
    public $customer;
    public $dwolla;
    public $wallet;
    public float $balance;

    public function __construct($user){
        $this->auth = app('firebase.auth');
        try{
            $this->user = $this->auth->getUser($user);
            $this->firestore = app('firebase.firestore');
            $this->db = $this->firestore->database();
            $this->uid = $this->user->uid;
        }
        catch(FirebaseException $e){
            // throw fatal error
        }
    }

    public function getPoints(){
        $transactions = [];

        $transactionDocs = $this->db->collection("Transactions");
        $transactionDocs = $transactionDocs
                ->where("uid","=",$this->uid)
                ->documents();
        // I need a separate firebase instance for sandboxing

        foreach($transactionDocs as $transactionDoc){
            array_push($transactions,  $transactionDoc->data());
        }

        return getPointsBalance($transactions);
    }

    public function customer(){
        $this->dwolla = new Dwolla;
        $this->customer = $this->dwolla->getCustomer($this->uid);
        return $this;
    }

    public function wallet(){
        // can only be called after customer is defined
        $this->wallet = $this->dwolla->getWallet($this->customer["customerID"]);
        return $this;
    }


    public function getCashBalance(){
        $wallet = $this->customer()->wallet();
        $walletDetails = $this->dwolla->getFundingSourceBalance($this->wallet->_links->self->href);
        $this->balance = (float) $walletDetails->balance->value;
        return $this->balance;
    }

    public function transactions(){
        $transactionDocs = $this->db->collection("Transactions")
            ->where("type","=","points-deposit")
            ->where("uid","=",$this->uid)
            ->orderBy("timestamp","DESC")
            ->documents();

        foreach($transactionDocs as $transactionDoc){
            array_push($this->transactions, $transactionDoc->data());
        }

        return $this;
    }

    public function canPointsPurchasePoints(){
        $transactions = $this->db->collection("Transactions")
            ->where("type","=","points-deposit")
            ->where("uid","=",$this->uid)
            ->orderBy("timestamp","DESC")
            ->documents();

        $now = Carbon::now();

        $numTransactions = 0;

        foreach($transactions as $transaction){
            $transaction = $transaction->data();
            if(str_contains($transaction["note"], "Points Purchase") && $transaction["from"] == "Venti"){
                $transactionDif = Carbon::parse($transaction["timestamp"])->diffInDays($now);

                if($transactionDif < 7){
                    return false;
                }
            }
            $numTransactions++;
        }

        return true;
    }
}
