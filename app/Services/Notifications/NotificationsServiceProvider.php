<?php

namespace App\Services\Notifications;

use Illuminate\Support\ServiceProvider;

class NotificationsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Notifications', function($app) {
            return new MailGun();
        });
    }
}