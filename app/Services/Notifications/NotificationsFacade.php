<?php

namespace App\Services\Notifications;

use Illuminate\Support\Facades\Facade;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Google\Cloud\Firestore\FirestoreClient;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Contract\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use SKAgarwal\GoogleApi\PlacesApi;
use Kreait\Firebase\Messaging\Notification as Notification;
use Kreait\Firebase\Messaging\ApnsConfig as ApnsConfig;

class NotificationsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Notifications';
    }

    // $recipient = the person that will be receiving the push notification
    // $sender = the person that created the action resulting in the push notification
    // $db = depending on where the function is called, we may not need to re-initiatd a database connection
    // $recipient and $sender are always getUser() objects
    // $group is always an array

    /*
        NOTIFICATION TYPES

        Someone requested to join my group
        I requested to join someone else's group, and they approved it.
        Someone liked a postcard that I published.
        Someone added a postcard for a destination that is on my wishlist.
        Someone added a postcard for a destination that I created a group for.
        Someone invited me to join their group
        A new message (other than my own) was added to a group I'm interested in.
        Someone added a destination to their wishlist that is also on my wishlist. new_wishlist_match
        Someone added a destination to their wishlist that matches a destination for one of my groups. new_wishlist_match
        Someone created a group for a destination that is on my wishlist. new_group_wishlist_match
        The venti team has published a system-wide announcement.
        Someone joined my group, but I'm not the owner of the group.

    */

    public static function tokenAuth($recipient, $db = null){
        if(is_null($db)){
            $firestore = app('firebase.firestore');
            $db = $firestore->database();  
        }
        
        // we need to check if the user being targeted has authorized push notifications to their device
        $tokenRef = $db->collection("users/" . $recipient->uid . "/Tokens")->document("data")->snapshot()->data();

        if($tokenRef == null || !isset($tokenRef)){
            return false; // no point in continuing because the recipient does not have an 
        }

        return $tokenRef; // needed for the actual message object
    }

    public static function permissionAuth($recipient, $notificationType, $db){
        if(is_null($db)){
            $firestore = app('firebase.firestore');
            $db = $firestore->database();  
        }

        $passThrough = true;

        // by default, all users are to receive all notifications unless they opt out.

        $settingsRef = $db->collection("users/" . $recipient->uid . "/Settings")->document("data")->snapshot()->data();

        // we'll only get to this step if we've checked if we have permission to send a push notification

        if($settingsRef != null || isset($settingsRef)){
            // the user has gone in to change at least one setting. So we have to check permissions one by one.
            $passThrough = false;
        }

        $message = null;
        $title = null;

        switch ($notificationType) {
            case "invited_to_trip":
                if($passThrough || $settingsRef["emailInvited"] == true){
                    return true;
                }
                
                break;
            case "requested_to_join_trip":
                if($passThrough || $settingsRef["emailJoinRequestFromOthers"] == true){
                    return true;
                }
                break;
            case "request_to_join_trip_approved":
                if($passThrough || $settingsRef["emailInviteAccepted"] == true){
                    return true;
                }
                break;
            case "invitation_to_join_trip_accepted":
                if($passThrough || $settingsRef["emailInviteAccepted"] == true){
                    return true;
                }
                break;
            case "new_member_added":
                if($passThrough || $settingsRef["emailNewMemberAdded"] == true){
                    return true;
                }
                break;
            case "new_group_message":
                if($passThrough || $settingsRef["groupMessage"] != false){
                    // will need a settings option for when the owner doesn't want this
                    return true;
                }
                break;
            case "postcard_like":
                if($passThrough || $settingsRef["postcardLike"] != false){
                    // will need a settings option for when the owner doesn't want this
                    return true;
                }
                break;
            case "new_wishlist_match":
                if($passThrough || $settingsRef["wishlistMatch"] != false){
                    // will need a settings option for when the owner doesn't want this
                    return true;
                }
                break;
            case "new_group_wishlist_match":
                if($passThrough || $settingsRef["groupWishlistMatch"] != false){
                    // will need a settings option for when the owner doesn't want this
                    return true;
                }
                break;
            default:
                return false;
                break;
        }

    }

     public static function invitedToTrip($recipient, $sender, $group, $db = null){
        // Someone invited me to join their group

        $notification = new \Notifications;

        $tokenRef = $notification->tokenAuth($recipient, $db);

        if(!$tokenRef){
            // abort. No device token to send notifiction
            return false;
        }

        if(!$notification->permissionAuth($recipient, "invited_to_trip", $db)){
            // abort. The user has turned off push notifications for this specified type
            return false;
        }

        $body = getFirstName($sender->displayName) . ' is inviting you to their group: ' . $group["title"];

        
        $message = CloudMessage::withTarget('token', $tokenRef["id"])
                    ->withNotification(Notification::create("You're Invited!", $body));

        if(!is_null($message)){
            $messaging = app('firebase.messaging');
            $messaging->send($message);
            // push notification sent
            return true;
        }

        // unable to send a push notification
        return false; 
    }

    public static function requestToJoinSomeonesGroup($recipient, $sender, $group, $db = null){
        // Someone requested to join my group

        $notification = new \Notifications;

        $tokenRef = $notification->tokenAuth($recipient, $db);

        if(!$tokenRef){
            // abort. No device token to send notifiction
            return false;
        }

        if(!$notification->permissionAuth($recipient, "requested_to_join_trip", $db)){
            // abort. The user has turned off push notifications for this specified type
            return false;
        }

        $destination = getTripLocation($group["destination"], $group["country"]);

        $body = getFirstName($sender->displayName) . " requested to join your group to $destination: " . $group["title"];

        
        $message = CloudMessage::withTarget('token', $tokenRef["id"])->withNotification(Notification::create("Someone's Interested", $body));

        if(!is_null($message)){
            $messaging = app('firebase.messaging');
            $messaging->send($message);
            // push notification sent
            return true;
        }

        // unable to send a push notification
        return false; 
    }

    public function requestToJoinGroupApproved($recipient, $sender, $group, $db = null){
        // Someone requested to join my group

        $notification = new \Notifications;

        $tokenRef = $notification->tokenAuth($recipient, $db);

        if(!$tokenRef){
            // abort. No device token to send notifiction
            return false;
        }

        if(!$notification->permissionAuth($recipient, "request_to_join_trip_approved", $db)){
            // abort. The user has turned off push notifications for this specified type
            return false;
        }

        $body = 'Your request to join ' . $group["title"] . ' has been approved';

        $message = CloudMessage::withTarget('token', $tokenRef["id"])->withNotification(Notification::create("You're In!", $body));

        if(!is_null($message)){
            $messaging = app('firebase.messaging');
            $messaging->send($message);
            // push notification sent
            return true;
        }

        // unable to send a push notification
        return false;
    }

    public function inviteAccepted($recipient, $sender, $group, $db = null){
        // Someone requested to join my group

        $notification = new \Notifications;

        $tokenRef = $notification->tokenAuth($recipient, $db);

        if(!$tokenRef){
            // abort. No device token to send notifiction
            return false;
        }

        if(!$notification->permissionAuth($recipient, "invitation_to_join_trip_accepted", $db)){
            // abort. The user has turned off push notifications for this specified type
            return false;
        }

        $body = getFirstName($sender->displayName) . ' accepted your invite to join: ' . $group["title"];

        $message = CloudMessage::withTarget('token', $tokenRef["id"])
                    ->withNotification(Notification::create('Invitation Accepted', $body));

        if(!is_null($message)){
            $messaging = app('firebase.messaging');
            $messaging->send($message);
            // push notification sent
            return true;
        }

        // unable to send a push notification
        return false;
    }

    public static function newMemberAdded($recipient, $sender, $group, $member, $db = null){
        // technically, the author of the trip is the sender of this notification, since the action flow already includes their data

        $notification = new \Notifications;

        $tokenRef = $notification->tokenAuth($recipient, $db);
        
        if(!$tokenRef){
            // abort. No device token to send notifiction
            return false;
        }


        if(!$notification->permissionAuth($recipient, "new_member_added", $db)){
            // abort. The user has turned off push notifications for this specified type
            return false;
        }

        
        $body = getFirstName($member->displayName) . " was added to your group: " . $group["title"];
        // 

            $message = CloudMessage::withTarget('token', $tokenRef["id"])
                ->withNotification(Notification::create("Someone New Joined", $body));

        if(!is_null($message)){
            $messaging = app('firebase.messaging');
            $messaging->send($message);
            // push notification sent
            return true;
        }

        // unable to send a push notification
        return false; 
    }

    public static function newGroupMessage($recipient, $sender, $group, $message, $db = null){
        

        $notification = new \Notifications;

        $tokenRef = $notification->tokenAuth($recipient, $db);
        
        if(!$tokenRef){
            // abort. No device token to send notifiction
            return false;
        }


        if(!$notification->permissionAuth($recipient, "new_group_message", $db)){
            // abort. The user has turned off push notifications for this specified type
            return false;
        }

        
        $body = getFirstName($sender->displayName) . ": $message";

            $message = CloudMessage::withTarget('token', $tokenRef["id"])
                ->withNotification(Notification::create($group["title"], $body));

        if(!is_null($message)){
            $messaging = app('firebase.messaging');
            $messaging->send($message);
            // push notification sent
            return true;
        }

        // unable to send a push notification
        return false; 
    }

    public static function postcardLike($recipient, $sender, $post, $db = null){
        // $recipient and $sender are always getUser() objects

        $notification = new \Notifications;

        $tokenRef = $notification->tokenAuth($recipient, $db);
        
        if(!$tokenRef){
            // abort. No device token to send notifiction
            return false;
        }


        if(!$notification->permissionAuth($recipient, "postcard_like", $db)){
            // abort. The user has turned off push notifications for this specified type
            return false;
        }

        $body = getFirstName($sender->displayName) . " liked your post: " . $post->title;

        
        $message = CloudMessage::withTarget('token', $tokenRef["id"])
            ->withNotification(Notification::create('You Got a Like!', $body));

        if(!is_null($message)){
            $messaging = app('firebase.messaging');
            $messaging->send($message);
            // push notification sent
            return true;
        }

        // unable to send a push notification
        return false; 
        
    }

    public static function newWishlistMatch($recipient, $sender, $destination, $db = null){

        $notification = new \Notifications;

        $tokenRef = $notification->tokenAuth($recipient, $db);
        
        if(!$tokenRef){
            // abort. No device token to send notifiction
            return false;
        }

        if(!$notification->permissionAuth($recipient, "new_wishlist_match", $db)){
            // abort. The user has turned off push notifications for this specified type
            return false;
        }

        $body = getFirstName($sender->displayName) . " added $destination to their wishlist";

        $message = CloudMessage::withTarget('token', $tokenRef["id"])
            ->withNotification(Notification::create('New travel buddy?', $body));

        if(!is_null($message)){
            $messaging = app('firebase.messaging');
            $messaging->send($message);
            // push notification sent
            return true;
        }

        // unable to send a push notification
        return false; 
        
    }

    public static function GroupWishlistMatch($recipient, $sender, $destination, $db = null){
        // 

        $notification = new \Notifications;

        $tokenRef = $notification->tokenAuth($recipient, $db);
        
        if(!$tokenRef){
            // abort. No device token to send notifiction
            return false;
        }

        if(!$notification->permissionAuth($recipient, "new_group_wishlist_match", $db)){
            // abort. The user has turned off push notifications for this specified type
            return false;
        }

        $body = getFirstName($sender->displayName) . " created a new travel group for $destination. It's on your wishlist!";

        $message = CloudMessage::withTarget('token', $tokenRef["id"])
            ->withNotification(Notification::create('New travel buddy?', $body));

        if(!is_null($message)){
            $messaging = app('firebase.messaging');
            $messaging->send($message);
            // push notification sent
            return true;
        }

        // unable to send a push notification
        return false; 
        
    }


}