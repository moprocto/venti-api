<?php

namespace App\Services\Dwolla;

use Illuminate\Support\ServiceProvider;

class DwollaServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Dwolla', function($app) {
            return new MailGun();
        });
    }
}